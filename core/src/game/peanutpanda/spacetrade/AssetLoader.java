package game.peanutpanda.spacetrade;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class AssetLoader {

    public final AssetManager assetManager = new AssetManager();

    public void startLoading() {
        loadFontHandling();

        assetManager.load("hudBack.png", Texture.class);
        assetManager.load("hudBackLarge.png", Texture.class);
        assetManager.load("background.png", Texture.class);
        assetManager.load("infoBack.png", Texture.class);
        assetManager.load("marketLabel.png", Texture.class);
        assetManager.load("planets.png", Texture.class);
        assetManager.load("planetsReverse.png", Texture.class);
        assetManager.load("ship.png", Texture.class);
        assetManager.load("title.png", Texture.class);
        assetManager.load("particle.png", Texture.class);

        assetManager.load("slots/apple.png", Texture.class);
        assetManager.load("slots/bell.png", Texture.class);
        assetManager.load("slots/cherry.png", Texture.class);
        assetManager.load("slots/clover.png", Texture.class);
        assetManager.load("slots/diamond.png", Texture.class);
        assetManager.load("slots/dollar.png", Texture.class);
        assetManager.load("slots/seven.png", Texture.class);
        assetManager.load("slots/shoe.png", Texture.class);
        assetManager.load("slots/back.png", Texture.class);

        assetManager.load("sounds/mainTheme.ogg", Music.class);

        assetManager.load("sounds/moneyWin.ogg", Sound.class);
        assetManager.load("sounds/moneyLose.ogg", Sound.class);
        assetManager.load("sounds/upgrade.ogg", Sound.class);
        assetManager.load("sounds/engine.ogg", Sound.class);

        loadSkins();
    }

    public void loadSkins() {
        SkinLoader.SkinParameter params = new SkinLoader.SkinParameter("trader.atlas");
        assetManager.load("trader.json", Skin.class, params);
    }

    public void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter gameboy = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        gameboy.fontFileName = "fonts/gameboy.ttf";
        gameboy.fontParameters.size = 20;
        gameboy.fontParameters.color = Color.GREEN;
        assetManager.load("gameboy.ttf", BitmapFont.class, gameboy);
    }

    public Texture particle() {
        return assetManager.get("particle.png", Texture.class);
    }

    public Sound moneyLose() {
        return assetManager.get("sounds/moneyLose.ogg", Sound.class);
    }

    public Sound moneyWin() {
        return assetManager.get("sounds/moneyWin.ogg", Sound.class);
    }

    public Sound upgrade() {
        return assetManager.get("sounds/upgrade.ogg", Sound.class);
    }

    public Sound engine() {
        return assetManager.get("sounds/engine.ogg", Sound.class);
    }

    public Music theme() {
        return assetManager.get("sounds/mainTheme.ogg", Music.class);
    }

    public Skin skin() {
        return assetManager.get("trader.json", Skin.class);
    }

    public Texture title() {
        return assetManager.get("title.png", Texture.class);
    }

    public Texture hudBack() {
        return assetManager.get("hudBack.png", Texture.class);
    }

    public Texture hudBackLarge() {
        return assetManager.get("hudBackLarge.png", Texture.class);
    }

    public Texture background() {
        return assetManager.get("background.png", Texture.class);
    }

    public Texture infoBack() {
        return assetManager.get("infoBack.png", Texture.class);
    }

    public Texture marketLabel() {
        return assetManager.get("marketLabel.png", Texture.class);
    }

    public Texture planets() {
        return assetManager.get("planets.png", Texture.class);
    }

    public Texture planetsReverse() {
        return assetManager.get("planetsReverse.png", Texture.class);
    }

    public Texture ship() {
        return assetManager.get("ship.png", Texture.class);
    }


    public Texture slotsApple() {
        return assetManager.get("slots/apple.png", Texture.class);
    }

    public Texture slotsBell() {
        return assetManager.get("slots/bell.png", Texture.class);
    }

    public Texture slotsCherry() {
        return assetManager.get("slots/cherry.png", Texture.class);
    }

    public Texture slotsClover() {
        return assetManager.get("slots/clover.png", Texture.class);
    }

    public Texture slotsDiamond() {
        return assetManager.get("slots/diamond.png", Texture.class);
    }

    public Texture slotsDollar() {
        return assetManager.get("slots/dollar.png", Texture.class);
    }

    public Texture slotsSeven() {
        return assetManager.get("slots/seven.png", Texture.class);
    }

    public Texture slotsShoe() {
        return assetManager.get("slots/shoe.png", Texture.class);
    }

    public Texture slotsBackground() {
        return assetManager.get("slots/back.png", Texture.class);
    }
}
