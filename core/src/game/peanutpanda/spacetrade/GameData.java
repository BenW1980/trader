package game.peanutpanda.spacetrade;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.gameobjects.Commodity;
import game.peanutpanda.spacetrade.gameobjects.Planet;
import game.peanutpanda.spacetrade.gameobjects.Player;
import game.peanutpanda.spacetrade.gameobjects.Vars;

public class GameData {

    private Preferences prefs;
    private Player player;

    public GameData() {
        prefs = Gdx.app.getPreferences("game-prefs");
        player = new Player();
    }

    public void saveHighscore(long score) {
        if (score > highscore()) {
            prefs.putLong("highscore", score);
            prefs.flush();
        }
    }

    public long highscore() {
        return prefs.getLong("highscore", 0);
    }


    public void setContinueButton(boolean flag) {
        prefs.putBoolean("continueOn", flag);

    }

    public void saveGame(Vars vars, Player player) {
        setContinueButton(true);
        savePlayer(player);
        savePlanets(vars);
        saveCommodities(vars);
        prefs.flush();
    }

    public void savePlayer(Player player) {
        prefs.putLong("money", player.getMoney());
        prefs.putInteger("taxesOwed", player.getTaxesOwed());
        prefs.putInteger("taxRate", player.getTaxRate());
        prefs.putInteger("cargoHoldSize", player.getCargoHoldSize());
        prefs.putInteger("cargoOnBoard", player.getCargoOnBoard());

        prefs.putInteger("week", player.getWeek());
        prefs.putInteger("amountToAdd", player.getAmountToAdd());
        prefs.putInteger("cargoLevel", player.getCargoLevel());
        prefs.putInteger("timesSpun", player.getTimesSpun());

        prefs.putBoolean("upgradeBought", player.isUpgradeBought());
        prefs.putBoolean("playedMaxTimes", player.isPlayedMaxTimes());
        prefs.putBoolean("playedLotteryThisWeek", player.hasPlayedLotteryThisWeek());
        prefs.putBoolean("travelledFromBelow", player.hasTravelledFromBelow());
        prefs.putBoolean("wishedForFortune", player.hasWishedForFortune());

        prefs.putString("playerPlanetName", player.getCurrentPlanet().getPlanetName());
        prefs.putString("playerPlanetFaction", player.getCurrentPlanet().getFactionName());
        prefs.putString("playerPlanetType", player.getCurrentPlanet().getType());

    }

    public Player loadPlayer() {

        player.setMoney(prefs.getLong("money"));
        player.setCargoHoldSize(prefs.getInteger("cargoHoldSize"));
        player.setCargoOnBoard(prefs.getInteger("cargoOnBoard"));

        player.setTaxesOwed(prefs.getInteger("taxesOwed"));
        player.setTaxRate(prefs.getInteger("taxRate"));

        player.setWeek(prefs.getInteger("week"));
        player.setAmountToAdd(prefs.getInteger("amountToAdd"));
        player.setCargoLevel(prefs.getInteger("cargoLevel"));
        player.setTimesSpun(prefs.getInteger("timesSpun"));

        player.setUpgradeBought(prefs.getBoolean("upgradeBought"));
        player.setPlayedMaxTimes(prefs.getBoolean("playedMaxTimes"));
        player.setPlayedLotteryThisWeek(prefs.getBoolean("playedLotteryThisWeek"));
        player.setTravelledFromBelow(prefs.getBoolean("travelledFromBelow"));
        player.setWishedForFortune(prefs.getBoolean("wishedForFortune"));

        return player;
    }

    public void savePlanets(Vars vars) {

        prefs.putString("planet1Name", vars.planets.get(0).getPlanetName());
        prefs.putString("planet1Faction", vars.planets.get(0).getFactionName());
        prefs.putString("planet1Special", vars.planets.get(0).getType());

        prefs.putString("planet2Name", vars.planets.get(1).getPlanetName());
        prefs.putString("planet2Faction", vars.planets.get(1).getFactionName());
        prefs.putString("planet2Special", vars.planets.get(1).getType());

        prefs.putString("planet3Name", vars.planets.get(2).getPlanetName());
        prefs.putString("planet3Faction", vars.planets.get(2).getFactionName());
        prefs.putString("planet3Special", vars.planets.get(2).getType());

        prefs.putString("planet4Name", vars.planets.get(3).getPlanetName());
        prefs.putString("planet4Faction", vars.planets.get(3).getFactionName());
        prefs.putString("planet4Special", vars.planets.get(3).getType());

        prefs.putString("planet5Name", vars.planets.get(4).getPlanetName());
        prefs.putString("planet5Faction", vars.planets.get(4).getFactionName());
        prefs.putString("planet5Special", vars.planets.get(4).getType());

        prefs.putString("planet6Name", vars.planets.get(5).getPlanetName());
        prefs.putString("planet6Faction", vars.planets.get(5).getFactionName());
        prefs.putString("planet6Special", vars.planets.get(5).getType());

        prefs.putString("planet7Name", vars.planets.get(6).getPlanetName());
        prefs.putString("planet7Faction", vars.planets.get(6).getFactionName());
        prefs.putString("planet7Special", vars.planets.get(6).getType());

        prefs.putString("planet8Name", vars.planets.get(7).getPlanetName());
        prefs.putString("planet8Faction", vars.planets.get(7).getFactionName());
        prefs.putString("planet8Special", vars.planets.get(7).getType());


    }

    public Array<Planet> loadPlanets() {

        Array<Planet> planetsToLoad = new Array<>();

        planetsToLoad.add(new Planet(prefs.getString("planet1Name"), prefs.getString("planet1Faction"), prefs.getString("planet1Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet2Name"), prefs.getString("planet2Faction"), prefs.getString("planet2Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet3Name"), prefs.getString("planet3Faction"), prefs.getString("planet3Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet4Name"), prefs.getString("planet4Faction"), prefs.getString("planet4Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet5Name"), prefs.getString("planet5Faction"), prefs.getString("planet5Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet6Name"), prefs.getString("planet6Faction"), prefs.getString("planet6Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet7Name"), prefs.getString("planet7Faction"), prefs.getString("planet7Special")));
        planetsToLoad.add(new Planet(prefs.getString("planet8Name"), prefs.getString("planet8Faction"), prefs.getString("planet8Special")));

        for (Planet planet : planetsToLoad) {
            if (prefs.getString("playerPlanetName").equalsIgnoreCase(planet.getPlanetName())) {
                player.setCurrentPlanet(planet);
            }
        }

        return planetsToLoad;
    }

    public void saveCommodities(Vars vars) {
        prefs.putString("comm1Name", vars.commodities.get(0).getCommodityName());
        prefs.putInteger("comm1OnPlanet", vars.commodities.get(0).getAmountOnPlanet());
        prefs.putInteger("comm1OnShip", vars.commodities.get(0).getAmountOnShip());
        prefs.putInteger("comm1Price", vars.commodities.get(0).getPrice());

        prefs.putString("comm2Name", vars.commodities.get(1).getCommodityName());
        prefs.putInteger("comm2OnPlanet", vars.commodities.get(1).getAmountOnPlanet());
        prefs.putInteger("comm2OnShip", vars.commodities.get(1).getAmountOnShip());
        prefs.putInteger("comm2Price", vars.commodities.get(1).getPrice());

        prefs.putString("comm3Name", vars.commodities.get(2).getCommodityName());
        prefs.putInteger("comm3OnPlanet", vars.commodities.get(2).getAmountOnPlanet());
        prefs.putInteger("comm3OnShip", vars.commodities.get(2).getAmountOnShip());
        prefs.putInteger("comm3Price", vars.commodities.get(2).getPrice());

        prefs.putString("comm4Name", vars.commodities.get(3).getCommodityName());
        prefs.putInteger("comm4OnPlanet", vars.commodities.get(3).getAmountOnPlanet());
        prefs.putInteger("comm4OnShip", vars.commodities.get(3).getAmountOnShip());
        prefs.putInteger("comm4Price", vars.commodities.get(3).getPrice());

        prefs.putString("comm5Name", vars.commodities.get(4).getCommodityName());
        prefs.putInteger("comm5OnPlanet", vars.commodities.get(4).getAmountOnPlanet());
        prefs.putInteger("comm5OnShip", vars.commodities.get(4).getAmountOnShip());
        prefs.putInteger("comm5Price", vars.commodities.get(4).getPrice());

        prefs.putString("comm6Name", vars.commodities.get(5).getCommodityName());
        prefs.putInteger("comm6OnPlanet", vars.commodities.get(5).getAmountOnPlanet());
        prefs.putInteger("comm6OnShip", vars.commodities.get(5).getAmountOnShip());
        prefs.putInteger("comm6Price", vars.commodities.get(5).getPrice());

        prefs.putString("comm7Name", vars.commodities.get(6).getCommodityName());
        prefs.putInteger("comm7OnPlanet", vars.commodities.get(6).getAmountOnPlanet());
        prefs.putInteger("comm7OnShip", vars.commodities.get(6).getAmountOnShip());
        prefs.putInteger("comm7Price", vars.commodities.get(6).getPrice());

        prefs.putString("comm8Name", vars.commodities.get(7).getCommodityName());
        prefs.putInteger("comm8OnPlanet", vars.commodities.get(7).getAmountOnPlanet());
        prefs.putInteger("comm8OnShip", vars.commodities.get(7).getAmountOnShip());
        prefs.putInteger("comm8Price", vars.commodities.get(7).getPrice());

        prefs.putString("comm9Name", vars.commodities.get(8).getCommodityName());
        prefs.putInteger("comm9OnPlanet", vars.commodities.get(8).getAmountOnPlanet());
        prefs.putInteger("comm9OnShip", vars.commodities.get(8).getAmountOnShip());
        prefs.putInteger("comm9Price", vars.commodities.get(8).getPrice());

        prefs.putString("comm10Name", vars.commodities.get(9).getCommodityName());
        prefs.putInteger("comm10OnPlanet", vars.commodities.get(9).getAmountOnPlanet());
        prefs.putInteger("comm10OnShip", vars.commodities.get(9).getAmountOnShip());
        prefs.putInteger("comm10Price", vars.commodities.get(9).getPrice());


    }

    public Array<Commodity> loadCommodities() {

        Array<Commodity> commoditiesToLoad = new Array<Commodity>();

        commoditiesToLoad.add(new Commodity(prefs.getString("comm1Name")));
        commoditiesToLoad.get(0).setAmountOnPlanet(prefs.getInteger("comm1OnPlanet"));
        commoditiesToLoad.get(0).setAmountOnShip(prefs.getInteger("comm1OnShip"));
        commoditiesToLoad.get(0).setPrice(prefs.getInteger("comm1Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm2Name")));
        commoditiesToLoad.get(1).setAmountOnPlanet(prefs.getInteger("comm2OnPlanet"));
        commoditiesToLoad.get(1).setAmountOnShip(prefs.getInteger("comm2OnShip"));
        commoditiesToLoad.get(1).setPrice(prefs.getInteger("comm2Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm3Name")));
        commoditiesToLoad.get(2).setAmountOnPlanet(prefs.getInteger("comm3OnPlanet"));
        commoditiesToLoad.get(2).setAmountOnShip(prefs.getInteger("comm3OnShip"));
        commoditiesToLoad.get(2).setPrice(prefs.getInteger("comm3Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm4Name")));
        commoditiesToLoad.get(3).setAmountOnPlanet(prefs.getInteger("comm4OnPlanet"));
        commoditiesToLoad.get(3).setAmountOnShip(prefs.getInteger("comm4OnShip"));
        commoditiesToLoad.get(3).setPrice(prefs.getInteger("comm4Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm5Name")));
        commoditiesToLoad.get(4).setAmountOnPlanet(prefs.getInteger("comm5OnPlanet"));
        commoditiesToLoad.get(4).setAmountOnShip(prefs.getInteger("comm5OnShip"));
        commoditiesToLoad.get(4).setPrice(prefs.getInteger("comm5Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm6Name")));
        commoditiesToLoad.get(5).setAmountOnPlanet(prefs.getInteger("comm6OnPlanet"));
        commoditiesToLoad.get(5).setAmountOnShip(prefs.getInteger("comm6OnShip"));
        commoditiesToLoad.get(5).setPrice(prefs.getInteger("comm6Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm7Name")));
        commoditiesToLoad.get(6).setAmountOnPlanet(prefs.getInteger("comm7OnPlanet"));
        commoditiesToLoad.get(6).setAmountOnShip(prefs.getInteger("comm7OnShip"));
        commoditiesToLoad.get(6).setPrice(prefs.getInteger("comm7Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm8Name")));
        commoditiesToLoad.get(7).setAmountOnPlanet(prefs.getInteger("comm8OnPlanet"));
        commoditiesToLoad.get(7).setAmountOnShip(prefs.getInteger("comm8OnShip"));
        commoditiesToLoad.get(7).setPrice(prefs.getInteger("comm8Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm9Name")));
        commoditiesToLoad.get(8).setAmountOnPlanet(prefs.getInteger("comm9OnPlanet"));
        commoditiesToLoad.get(8).setAmountOnShip(prefs.getInteger("comm9OnShip"));
        commoditiesToLoad.get(8).setPrice(prefs.getInteger("comm9Price"));

        commoditiesToLoad.add(new Commodity(prefs.getString("comm10Name")));
        commoditiesToLoad.get(9).setAmountOnPlanet(prefs.getInteger("comm10OnPlanet"));
        commoditiesToLoad.get(9).setAmountOnShip(prefs.getInteger("comm10OnShip"));
        commoditiesToLoad.get(9).setPrice(prefs.getInteger("comm10Price"));

        return commoditiesToLoad;

    }

    public Preferences getPrefs() {
        return prefs;
    }

}