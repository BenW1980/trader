package game.peanutpanda.spacetrade;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class NameGenerator {

    public Array<String> planetFirst;
    public Array<String> planetSecond;
    public Array<String> planetFull;

    public Array<String> factionFirst;
    public Array<String> factionSecond;
    public Array<String> factionThird;
    public Array<String> faction1_2;
    public Array<String> factionAllEnds;

    public Array<String> commoditYTier1;
    public Array<String> commoditYTier2;
    public Array<String> commoditYTier3;
    public Array<String> commoditYTier4;
    public Array<String> commoditYTier5;
    public Array<String> commoditYTier6;
    public Array<String> commoditYTier7;
    public Array<String> commoditYTier8;
    public Array<String> commoditYTier9;
    public Array<String> commoditYTier10;

    public NameGenerator() {
        planetFirst = new Array<>();
        planetSecond = new Array<>();
        planetFull = new Array<>();
        factionFirst = new Array<>();
        factionSecond = new Array<>();
        factionThird = new Array<>();
        faction1_2 = new Array<>();
        factionAllEnds = new Array<>();

        commoditYTier1 = new Array<>();
        commoditYTier2 = new Array<>();
        commoditYTier3 = new Array<>();
        commoditYTier4 = new Array<>();
        commoditYTier5 = new Array<>();
        commoditYTier6 = new Array<>();
        commoditYTier7 = new Array<>();
        commoditYTier8 = new Array<>();
        commoditYTier9 = new Array<>();
        commoditYTier10 = new Array<>();

        planetFirst.addAll("Hex", "Mint", "Hogg", "Tryx", "Slum", "Snot", "Snug",
                "Snort", "Vex", "Tron", "Qix", "Blart", "High", "Wuu", "Smoki", "Lop", "Kra",
                "Sma", "Smo", "Tork", "Urg", "Blerg", "Nos", "Vas", "Pok", "Athe", "Queg",
                "Ca", "Herm", "Mon", "Min", "Leto", "Tra", "Nau", "Abi", "Akra", "Lao", "Crom",
                "Oon", "Nola", "Pots", "Low", "Mix", "Dork", "Smeg", "Borp", "Zox", "Moxy", "Circo",
                "Clor", "Klamp", "Ben", "Bale", "Hogg", "Va", "Rae", "Butt");

        planetSecond.addAll("", "", "", "", "", "", "", "", "om", "us", "as", "asus", "omnos",
                "ostrom", "yu", "yan", "yen", "ben", "ren", "plin", "tix", "tea",
                "aten", "astis", "wash", "iris", "siro", "agar", "pey", "dria", "opolis", "seum",
                "kratis", "dea", "gos", "ras", "pie", "pos", "o", "is", "drip", "fus", "fish", "tork", "une",
                "us", "to", "iter", "oid", "mans", "ness", "lus", "butt", "ox");

        factionFirst.addAll("The Sons of ", "The Kingdom of ", "The ", "The Bishopry of ", "The House of ",
                "The Magic ", "The Great ", "The Daughters of ", "The Enigmatic ",
                "The Front of ", "The Other ", "The Greater ", "The Reactive ", "The Empire of ",
                "The Holy ", "The Free ", "The Outer ", "The Inner ", "The Most Excellent ",
                "The Dirty ", "The Dominion of ", "The Fiefdom of ", "The Solar ", "The Far ",
                "The Peasantry of ", "The Church of ", "The Papal ", "The Further ",
                "The Ladies of ", "The Lords of ", "The Red ", "The Light Purple ", "The Fizzy ", "The Lands of ",
                "The First ", "The Second ", "The Collective of ", "The Blind ", "The Knights of ", "The Last ",
                "The Fantastic ", "The Mediocre ", "The Fat ", "The Happy ", "The Federation of ", "The Diamond ",
                "The Golden ", "The United ", "The Ministry of ", "The Princedom of ", "The Noble ",
                "The Makers of ", "The Sympathetic ", "The True ", "The Original ", "The Ultimate ",
                "The Kings of ", "The Queens of ", "The Union of ", "The Federal ", "The Theocratic ", "The Unholy ",
                "The Feudal ", "The Middle ", "The Border ", "The Colony of ", "The Terran ", "The Transhumanist ",
                "The Transcultural ", "The Probiotic ", "The Silent ", "The Imperial ", "The Communion of ", "The Cosmic ", "The Vegan ",
                "The Bamboo ", "The Nut ");

        factionSecond.addAll("Dave", "Sons", "Daughters", "Solars", "Ant People",
                "Kingdoms", "Peasants", "Sven",
                "Free Cupcakes", "Bishops", "Lesser Domains", "Planets", "Sea People",
                "Mole Men", "Unholy Princes", "Followers", "People", "Spacers", "Light", "Colour",
                "Accountancy", "Pie", "Emptiness", "Too Much", "Nothing", "Excellence", "Farmboys", "Farmgirls", "Farmers", "Life", "Mediocrity",
                "Bears", "Happiness", "Kings", "Queens", "Pirates", "Bakers", "Superiority", "Fishmongers", "Terrans",
                "Machines", "Warmongers", "Gamers", "Eaters", "Orion", "Bandits", "Menace", "Vegans", "Poachers");

        factionThird.addAll("Empire", "Dominion", "Republic", "Enigma", "Kingdom", "Collective",
                "Principality", "Peasantry", "Fiefdom", "Theocracy", "House", "Lands", "Plains",
                "Colonies", "Caretaker", "Network", "Belt", "Commonwealth", "Ones", "Transcendence", "Cooperation",
                "Sphere", "Organisation", "Culture", "Volumes", "Periphery", "Regions", "Vegans", "League", "Alliance",
                "Communion", "Gods", "Society"); //niet eindigend op 'of'

        for (String s : factionSecond) {
            factionAllEnds.add(s);
        }

        for (String s : factionThird) {
            factionAllEnds.add(s);
        }

        commoditYTier1.addAll("Water", "Ice", "Candy", "Peanuts", "Almonds", "Paper", "Sprouts", "Coconuts", "Berries");
        commoditYTier2.addAll("Toys", "Vegetable Oils", "Shrimp Juices", "Pumpkins", "Greens", "Reds", "Purples", "Mints", "Bamboo");
        commoditYTier3.addAll("Scrap Metal", "Designer Clothes", "Bits", "Bytes", "Buttons", "Pots and Pans", "Lemongrass");
        commoditYTier4.addAll("Fertilizer", "Towels", "Magnets", "Thimbleweeds", "Hoverboards", "Sneakers", "Threepwoods");
        commoditYTier5.addAll("Domestic Bots", "Farm Bots", "Farm Equipment", "Fedoras", "Leather Jackets", "Jalapenos", "Peppers");
        commoditYTier6.addAll("Medicine", "Tea", "Beans", "Essential Oils", "Inessential Oils", "Artifacts", "Iced Tea");
        commoditYTier7.addAll("Light Arms", "Silver Platters", "Exotic Plants", "Gold Platters");
        commoditYTier8.addAll("Kumquats", "Cupcakes", "Potatoes", "Niceties", "Grits", "Kibbles", "Apple Pies");
        commoditYTier9.addAll("Ship Parts", "Machine Parts", "Blankies", "Bronze Medals", "Ceramics");
        commoditYTier10.addAll("Platinum Bars", "Goldiniums", "Guybrushiums", "Mindiniums", "Vanessiums");

        generatePlanetNames();
        generateFactionNames();
    }

    public void generatePlanetNames() {
        for (int i = 0; i < 8; i++) {

            int first = MathUtils.random(0, planetFirst.size - 1);
            int second = MathUtils.random(0, planetSecond.size - 1);

            planetFull.add(planetFirst.get(first) + planetSecond.get(second));
            planetFirst.removeIndex(first);
            planetSecond.removeIndex(second);
        }
    }

    public void generateFactionNames() {
        for (int i = 0; i < 8; i++) {

            int first = MathUtils.random(0, factionFirst.size - 1);

            if (!factionFirst.get(first).endsWith("of ")) {
                int second = MathUtils.random(0, factionAllEnds.size - 1);
                faction1_2.add(factionFirst.get(first) + factionAllEnds.get(second));
                factionFirst.removeIndex(first);
                factionAllEnds.removeIndex(second);
            } else {
                int second = MathUtils.random(0, factionSecond.size - 1);
                faction1_2.add(factionFirst.get(first) + factionSecond.get(second));
                factionFirst.removeIndex(first);
                factionSecond.removeIndex(second);
            }
        }
    }
}
