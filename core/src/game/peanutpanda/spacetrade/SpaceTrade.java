package game.peanutpanda.spacetrade;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.peanutpanda.spacetrade.gameobjects.Vars;
import game.peanutpanda.spacetrade.screens.LoadingScreen;

public class SpaceTrade extends Game {

    public AssetLoader assLoader;
    public OrthographicCamera camera;
    public Viewport viewport;
    public Vars vars;
    public GameData gameData;

    @Override
    public void create() {
        this.assLoader = new AssetLoader();
        this.camera = new OrthographicCamera();
        this.camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.camera.update();
        this.gameData = new GameData();

        Gdx.input.setCatchBackKey(true);
        setScreen(new LoadingScreen(this));
    }
}
