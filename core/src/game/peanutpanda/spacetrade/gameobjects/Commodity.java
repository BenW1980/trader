package game.peanutpanda.spacetrade.gameobjects;


import com.badlogic.gdx.utils.Array;


public class Commodity {

    private String commodityName;
    private Planet planetBought;
    private int amountOnPlanet;
    private int amountOnShip;
    private int price;
    private Array<Integer> boughtAtPrice;
    public String shortCode;

    public Commodity(String commodityName) {
        this.commodityName = commodityName;
        boughtAtPrice = new Array<>();
        shortCode = "" + commodityName.charAt(0) + commodityName.charAt(1) + commodityName.charAt(commodityName.length() - 2);
    }

    public void transferToShipRandomEvent(int amount) {
        amountOnShip += amount;
    }

    public void transferToShip(int amount) {

        if (amount <= amountOnPlanet) {
            amountOnPlanet -= amount;
            amountOnShip += amount;
        } else {
            amountOnShip += amountOnPlanet;
            amountOnPlanet = 0;
        }
    }

    public void transferOneToShip() {
        amountOnPlanet--;
        amountOnShip++;
    }

    public void transferTenToShip() {
        amountOnPlanet -= 10;
        amountOnShip += 10;
    }

    public void transferOneToPlanet() {
        amountOnPlanet++;
        amountOnShip--;
    }

    public void transferTenToPlanet() {
        amountOnPlanet += 10;
        amountOnShip -= 10;
    }

    public void transferMaxToPlanet() {
        amountOnPlanet += amountOnShip;
        amountOnShip = 0;
    }

    public void addToBoughtAtPrice(int boughtAt, int times) {
        for (int i = 0; i < times; i++) {
            boughtAtPrice.add(boughtAt);
        }
    }

    public int calculateBoughtAtPrice() {
        double result = 0;

        for (Integer i : boughtAtPrice) {
            result += i;
        }

        if (boughtAtPrice.size != 0) {
            return (int) result / boughtAtPrice.size;
        }

        return (int) result;
    }

    public void emptyBoughtAtPrice() {
        boughtAtPrice.clear();
    }

    public String getCommodityName() {
        return commodityName;
    }

    public int getAmountOnPlanet() {
        return amountOnPlanet;
    }

    public void setAmountOnPlanet(int amountOnPlanet) {
        this.amountOnPlanet = amountOnPlanet;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmountOnShip() {
        return amountOnShip;
    }

    public void setAmountOnShip(int amountOnShip) {
        this.amountOnShip = amountOnShip;
    }

    public Planet getPlanetBought() {
        return planetBought;
    }

    public void setPlanetBought(Planet planetBought) {
        this.planetBought = planetBought;
    }
}
