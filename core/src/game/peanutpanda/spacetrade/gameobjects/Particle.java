package game.peanutpanda.spacetrade.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import game.peanutpanda.spacetrade.ScreenSize;

public class Particle {

    public Sprite sprite;
    public float speed, x, y;

    public Particle(Texture texture) {
        sprite = new Sprite(texture);
        sprite.setY(ScreenSize.HEIGHT.getSize());
        sprite.setX(MathUtils.random(10, ScreenSize.WIDTH.getSize() - 10));
        speed = MathUtils.random(1, 17);
    }

    public void move(float delta) {
        sprite.setY(sprite.getY() - speed * (delta * 20));
    }

}
