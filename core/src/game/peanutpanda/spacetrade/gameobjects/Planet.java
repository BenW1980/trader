package game.peanutpanda.spacetrade.gameobjects;


public class Planet {

    private String planetName;
    private String factionName;
    private String type;
    private String buttonDisplay;

    public Planet(String planetName, String factionName, String type) {
        this.planetName = planetName;
        this.factionName = factionName;
        this.type = type;
        this.buttonDisplay = setButtonDisplay(type);
    }

    public String setButtonDisplay(String name) {
        String txt = "";

        switch (name) {
            case "-Upgrade-":
                txt = "Upgrade";
                break;
            case "-Casino-":
                txt = "Casino";
                break;
            case "-Lottery-":
                txt = "Lottery";
                break;
            case "-Oracle-":
                txt = "Oracle";
                break;
            case "-Imperial Seat-":
                txt = "Visit";
                break;
        }

        return txt;
    }


    public String getPlanetName() {
        return planetName;
    }

    public String getFactionName() {
        return factionName;
    }

    public String getType() {
        return type;
    }

    public String getButtonDisplay() {
        return buttonDisplay;
    }
}