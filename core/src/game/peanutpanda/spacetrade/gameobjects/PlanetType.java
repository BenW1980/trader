package game.peanutpanda.spacetrade.gameobjects;

public enum PlanetType {

    UPGRADE("-Upgrade-"),
    CASINO("-Casino-"),
    LOTTERY("-Lottery-"),
    ORACLE("-Oracle-"),
    CAPITAL("-Imperial Seat-"),
    NONE("");

    private final String specialType;

    PlanetType(String type) {
        this.specialType = type;
    }

    public String getSpecialType() {
        return specialType;
    }
}
