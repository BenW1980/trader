package game.peanutpanda.spacetrade.gameobjects;

public class Player {

    public static final int MAX_WEEK = 40;

    private long money;

    private int taxesOwed;
    private int taxRate;

    private int cargoHoldSize;
    public int cargoOnBoard;
    private Planet currentPlanet;
    private int week;
    private int amountToAdd;
    private int cargoLevel;
    private int timesSpun;
    private boolean upgradeBought;
    private boolean playedMaxTimes;
    private boolean playedLotteryThisWeek;
    private boolean travelledFromBelow;
    private boolean wishedForFortune;
    private int taxUrgencyCounter;
    public boolean isWarnedAboutTaxes;

    public Player() {

    }

    public Player(Planet currentPlanet) {
        this.currentPlanet = currentPlanet;
        upgradeBought = false;
        playedMaxTimes = false;
        money = 250;
        cargoHoldSize = 25;
        cargoOnBoard = 0;
        taxesOwed = 0;
        week = 1;
        cargoLevel = 0;
        taxRate = 15;
    }

    public TaxUrgency calculateTaxUrgency() {

        TaxUrgency taxUrgency = TaxUrgency.SAFE;

        if (taxesOwed >= 100000) {
            taxUrgency = TaxUrgency.CRITICAL;
        } else if (taxesOwed > 50000 && taxesOwed <= 100000) {
            taxUrgency = TaxUrgency.URGENT;
        } else if (taxesOwed > 10000 && taxesOwed <= 50000) {
            taxUrgency = TaxUrgency.DUE;
        } else if (taxesOwed <= 10000) {
            taxUrgency = TaxUrgency.SAFE;
        }

        return taxUrgency;
    }

    public void addSpin() {
        this.timesSpun++;
    }

    public void goToNextWeek() {
        week++;
    }

    public boolean hasRoomLeft() {
        return cargoOnBoard < cargoHoldSize;
    }

    public boolean hasRoomLeftForTen() {
        return cargoOnBoard <= cargoHoldSize - 10;
    }

    public void buyMax(Commodity commodity) {
        int storageLeft = cargoHoldSize - cargoOnBoard;
        int totalPriceAllGoods = commodity.getPrice() * commodity.getAmountOnPlanet();
        int amountToBuy;

        if (totalPriceAllGoods <= money) {
            amountToBuy = Math.min(commodity.getAmountOnPlanet(), storageLeft);

        } else {
            amountToBuy = Math.min((int) money / commodity.getPrice(), storageLeft);
        }

        money -= amountToBuy * commodity.getPrice();
        commodity.transferToShip(amountToBuy);
        commodity.setPlanetBought(currentPlanet);
        this.amountToAdd = amountToBuy;
        cargoOnBoard += amountToBuy;
    }

    public void buyOne(Commodity commodity) {
        commodity.transferOneToShip();
        subtractMoney(commodity.getPrice());
        commodity.setPlanetBought(currentPlanet);
        cargoOnBoard++;
    }

    public void buyTen(Commodity commodity) {
        commodity.transferTenToShip();
        subtractMoney(commodity.getPrice() * 10);
        commodity.setPlanetBought(currentPlanet);
        cargoOnBoard += 10;
    }

    public void sellOne(Commodity commodity) {
        commodity.transferOneToPlanet();
        addMoney(commodity.getPrice());
        addTaxes(commodity.getPrice(), commodity);
        cargoOnBoard--;
    }

    public void sellTen(Commodity commodity) {
        commodity.transferTenToPlanet();
        addMoney(commodity.getPrice() * 10);
        addTaxes(commodity.getPrice() * 10, commodity);
        cargoOnBoard -= 10;
    }

    public void sellMax(Commodity commodity) {
        int totalPriceAllGoods = commodity.getAmountOnShip() * commodity.getPrice();
        money += totalPriceAllGoods;
        addTaxes(totalPriceAllGoods, commodity);
        cargoOnBoard -= commodity.getAmountOnShip();
        commodity.transferMaxToPlanet();
    }

    public void addTaxes(int amount, Commodity commodity) {
        if (commodity.getPlanetBought() != currentPlanet) {
            taxesOwed += (amount / 100) * taxRate;
        }
    }

    public void travelTo(Planet destination) {
        currentPlanet = destination;
    }

    public void addMoney(long amount) {
        this.money += amount;
    }

    public void subtractMoney(long amount) {
        this.money -= amount;
    }

    public void upgradeCargo(int amount) {
        this.cargoHoldSize = amount;
    }

    public Planet getCurrentPlanet() {
        return currentPlanet;
    }

    public void setCurrentPlanet(Planet currentPlanet) {
        this.currentPlanet = currentPlanet;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public int getAmountToAdd() {
        return amountToAdd;
    }

    public void setAmountToAdd(int amountToAdd) {
        this.amountToAdd = amountToAdd;
    }

    public int getCargoHoldSize() {
        return cargoHoldSize;
    }

    public void setCargoHoldSize(int cargoHoldSize) {
        this.cargoHoldSize = cargoHoldSize;
    }

    public int getCargoOnBoard() {
        return cargoOnBoard;
    }

    public void setCargoOnBoard(int cargoOnBoard) {
        this.cargoOnBoard = cargoOnBoard;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public boolean isUpgradeBought() {
        return upgradeBought;
    }

    public void setUpgradeBought(boolean upgradeBought) {
        this.upgradeBought = upgradeBought;
    }

    public int getCargoLevel() {
        return cargoLevel;
    }

    public void setCargoLevel(int cargoLevel) {
        this.cargoLevel = cargoLevel;
    }

    public boolean isPlayedMaxTimes() {
        return playedMaxTimes;
    }

    public void setPlayedMaxTimes(boolean playedMaxTimes) {
        this.playedMaxTimes = playedMaxTimes;
    }

    public int getTimesSpun() {
        return timesSpun;
    }

    public void setTimesSpun(int timesSpun) {
        this.timesSpun = timesSpun;
    }

    public boolean hasPlayedLotteryThisWeek() {
        return playedLotteryThisWeek;
    }

    public void setPlayedLotteryThisWeek(boolean playedLotteryThisWeek) {
        this.playedLotteryThisWeek = playedLotteryThisWeek;
    }

    public boolean hasTravelledFromBelow() {
        return travelledFromBelow;
    }

    public void setTravelledFromBelow(boolean travelledFromBelow) {
        this.travelledFromBelow = travelledFromBelow;
    }

    public boolean hasWishedForFortune() {
        return wishedForFortune;
    }

    public void setWishedForFortune(boolean wishedForFortune) {
        this.wishedForFortune = wishedForFortune;
    }

    public int getTaxesOwed() {
        return taxesOwed;
    }

    public void setTaxesOwed(int taxesOwed) {
        this.taxesOwed = taxesOwed;
    }

    public int getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(int taxRate) {
        this.taxRate = taxRate;
    }

    public int getTaxUrgencyCounter() {
        return taxUrgencyCounter;
    }

    public void setTaxUrgencyCounter(int taxUrgencyCounter) {
        this.taxUrgencyCounter = taxUrgencyCounter;
    }
}