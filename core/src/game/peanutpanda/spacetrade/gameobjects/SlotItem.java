package game.peanutpanda.spacetrade.gameobjects;


import com.badlogic.gdx.graphics.Texture;

import game.peanutpanda.spacetrade.AssetLoader;

public class SlotItem {

    private int id;
    private Texture texture;

    public SlotItem(SlotItemType type, AssetLoader assetLoader) {
        this.id = type.getId();
        this.texture = assignTexture(type, assetLoader);
    }

    private Texture assignTexture(SlotItemType type, AssetLoader assetLoader) {

        Texture texture = null;

        switch (type) {
            case APPLE:
                texture = assetLoader.slotsApple();
                break;
            case BELL:
                texture = assetLoader.slotsBell();
                break;
            case CHERRY:
                texture = assetLoader.slotsCherry();
                break;
            case CLOVER:
                texture = assetLoader.slotsClover();
                break;
            case DIAMOND:
                texture = assetLoader.slotsDiamond();
                break;
            case DOLLAR:
                texture = assetLoader.slotsDollar();
                break;
            case SEVEN:
                texture = assetLoader.slotsSeven();
                break;
            case SHOE:
                texture = assetLoader.slotsShoe();
                break;
        }
        return texture;
    }

    public int getId() {
        return id;
    }

    public Texture getTexture() {
        return texture;
    }
}
