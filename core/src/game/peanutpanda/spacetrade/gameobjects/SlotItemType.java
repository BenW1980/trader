package game.peanutpanda.spacetrade.gameobjects;


public enum SlotItemType {

    CHERRY(1), APPLE(2), SHOE(3), CLOVER(4), BELL(5), DIAMOND(6), DOLLAR(7), SEVEN(8);

    private final int id;

    SlotItemType(int size) {
        this.id = size;
    }

    public int getId() {
        return id;
    }
}