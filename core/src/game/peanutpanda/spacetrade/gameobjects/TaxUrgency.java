package game.peanutpanda.spacetrade.gameobjects;

public enum TaxUrgency {

    SAFE, DUE, URGENT, CRITICAL

}