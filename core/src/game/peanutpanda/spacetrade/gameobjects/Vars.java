package game.peanutpanda.spacetrade.gameobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.AssetLoader;
import game.peanutpanda.spacetrade.GameData;
import game.peanutpanda.spacetrade.NameGenerator;
import game.peanutpanda.spacetrade.ScreenSize;
import game.peanutpanda.spacetrade.random.Randomiser;

public class Vars {

    public Array<Planet> planets;
    public Array<Commodity> commodities;
    public Player player;
    public Randomiser rnd;
    public NameGenerator nameGenerator;
    public Array<Integer> cargoUpgradeCost;
    public Array<Integer> cargoUpgradeSize;

    public int jackpot;
    private GameData gameData;

    public Array<String> secretNumberArray;
    public Array<String> oracleWinningNumbers;

    public Array<Particle> particles;
    public float particleTimePassed;

    public long moneyToWin;

    public Vars(GameData gameData) {

        this.gameData = gameData;

        this.jackpot = MathUtils.random(20000, 30000);

        this.planets = new Array<>();
        this.commodities = new Array<>();
        this.rnd = new Randomiser();
        this.nameGenerator = new NameGenerator();
        this.cargoUpgradeCost = new Array<>();
        this.cargoUpgradeSize = new Array<>();

        cargoUpgradeCost.addAll(2500, 10000, 25000, 60000, 100000, 150000, 500000, 800000, 1200000);
        cargoUpgradeSize.addAll(50, 100, 200, 400, 750, 1000, 2000, 3000, 4000);

        oracleWinningNumbers = new Array<>();
        secretNumberArray = new Array<>();
        secretNumberArray.addAll("1", "2", "3", "4", "5", "6");

        moneyToWin = 20000000;


    }

    public void initParticleArrayPlanets(AssetLoader assetLoader) {
        particles = new Array<>();
        for (int i = 0; i < 5; i++) {
            particles.add(new Particle(assetLoader.particle()));
            particles.get(i).speed = MathUtils.random(1, 2);
            particles.get(i).sprite.setPosition(
                    MathUtils.random(10, ScreenSize.WIDTH.getSize() - 10), MathUtils.random(100, ScreenSize.HEIGHT.getSize() - 10));
        }
    }

    public void createParticlesPlanets(float delta, AssetLoader assetLoader) {
        particleTimePassed += delta * 5;

        if (particles.size > 50) {
            particles.clear();
        }

        if (particleTimePassed > 10) {
            particles.add(new Particle(assetLoader.particle()));
            particleTimePassed = 0;
        }
    }

    public void initParticleArray(AssetLoader assetLoader) {
        particles = new Array<>();
        for (int i = 0; i < 8; i++) {
            particles.add(new Particle(assetLoader.particle()));
            particles.get(i).sprite.setPosition(
                    MathUtils.random(10, ScreenSize.WIDTH.getSize() - 10), MathUtils.random(100, ScreenSize.HEIGHT.getSize() - 10));
        }
    }

    public void createParticles(float delta, AssetLoader assetLoader) {
        particleTimePassed += delta * 10;

        if (particles.size > 50) {
            particles.clear();
        }

        if (particleTimePassed > 10) {
            particles.add(new Particle(assetLoader.particle()));
            particleTimePassed = 0;
        }
    }

    public void renderParticles(Stage stage, float delta) {
        for (Particle particle : particles) {
            particle.sprite.draw(stage.getBatch());
            particle.move(delta);
        }
    }

    public void resetSecretNumber() {
        secretNumberArray.shuffle();
        oracleWinningNumbers.clear();
        oracleWinningNumbers = new Array<>();
    }

    public void createOracleNumbers() {
        resetSecretNumber();
        oracleWinningNumbers.add(secretNumberArray.get(0), secretNumberArray.get(1), secretNumberArray.get(3));
        oracleWinningNumbers.sort();
    }

    public void newGame() {
        planets.add(new Planet(nameGenerator.planetFull.get(0), nameGenerator.faction1_2.get(0), PlanetType.UPGRADE.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(1), nameGenerator.faction1_2.get(1), PlanetType.NONE.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(2), nameGenerator.faction1_2.get(2), PlanetType.CASINO.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(3), nameGenerator.faction1_2.get(3), PlanetType.NONE.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(4), nameGenerator.faction1_2.get(4), PlanetType.NONE.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(5), nameGenerator.faction1_2.get(5), PlanetType.LOTTERY.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(6), nameGenerator.faction1_2.get(6), PlanetType.CAPITAL.getSpecialType()));
        planets.add(new Planet(nameGenerator.planetFull.get(7), nameGenerator.faction1_2.get(7), PlanetType.ORACLE.getSpecialType()));

        commodities.add(new Commodity(nameGenerator.commoditYTier1.get(MathUtils.random(0, nameGenerator.commoditYTier1.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier2.get(MathUtils.random(0, nameGenerator.commoditYTier2.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier3.get(MathUtils.random(0, nameGenerator.commoditYTier3.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier4.get(MathUtils.random(0, nameGenerator.commoditYTier4.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier5.get(MathUtils.random(0, nameGenerator.commoditYTier5.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier6.get(MathUtils.random(0, nameGenerator.commoditYTier6.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier7.get(MathUtils.random(0, nameGenerator.commoditYTier7.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier8.get(MathUtils.random(0, nameGenerator.commoditYTier8.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier9.get(MathUtils.random(0, nameGenerator.commoditYTier9.size - 1))));
        commodities.add(new Commodity(nameGenerator.commoditYTier10.get(MathUtils.random(0, nameGenerator.commoditYTier10.size - 1))));

        player = new Player(planets.get(MathUtils.random(0, planets.size - 1)));

        planets.shuffle();
        rnd.randomiseCommodities(commodities);

    }

    public void loadGame() {

        planets = gameData.loadPlanets();
        commodities = gameData.loadCommodities();
        player = gameData.loadPlayer();

    }

    public void randomizeJackpot() {
        this.jackpot = MathUtils.random(20000, 24000) * player.getWeek();
    }

}