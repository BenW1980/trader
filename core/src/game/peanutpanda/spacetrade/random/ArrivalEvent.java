package game.peanutpanda.spacetrade.random;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.gameobjects.Commodity;
import game.peanutpanda.spacetrade.gameobjects.Player;
import game.peanutpanda.spacetrade.gameobjects.Vars;

public class ArrivalEvent {

    public Vars vars;
    public Player player;

    public ArrivalEvent(Vars vars, Player player) {
        this.vars = vars;
        this.player = player;
    }

    public String randomEvent() {

        int rnd = MathUtils.random(1, 9);
        String txt = "";

        switch (rnd) {
            case 1:
            case 2:
                txt = shortage();
                break;
            case 3:
                txt = surplus();
                break;
            case 4:
            case 5:
            case 6:
                txt = weather();
                break;
            case 7:
            case 8:
            case 9:
                txt = news();
                break;
        }

        return txt;

    }

    public String shortage() {

        Commodity c = vars.commodities.get(MathUtils.random(0, vars.commodities.size - 1));
        c.setPrice(c.getPrice() * MathUtils.random(5, 8));
        c.setAmountOnPlanet(MathUtils.random(0, 10));
        return randomShortageText(c);

    }

    public String surplus() {

        Commodity c = vars.commodities.get(MathUtils.random(0, vars.commodities.size - 1));
        c.setPrice(c.getPrice() / 5);
        c.setAmountOnPlanet(MathUtils.random(500, 800));
        return randomSurplusText(c);

    }

    private String news() {

        int rnd = MathUtils.random(1, 28);
        String txt = "";
        String planet = vars.planets.get(MathUtils.random(0, vars.planets.size - 1)).getPlanetName();
        String faction = player.getCurrentPlanet().getFactionName();

        Array<String> papers = new Array<>();
        papers.addAll("Bulletin Press", "Heritage Daily", "The Western Chronicle", "The Breakfast Times", "The Independent Tribune", "The Explorer Times",
                "The Northern Report", "The Beacon Tribune", "The Pioneer Chronicle", "The Insider Times", "The Sunrise Mirror",
                "The Diem Report", "Emerald Press", "The Dawn Observer", "Epoch Tribune", "Times Daily",
                "The Express Herald", "Look Back Weekly", "The Telegraph Telegram", "The Metropolitan Journal", "The Telegraph Inquirer", "Daily Watch News");

        String paper = papers.get(MathUtils.random(0, papers.size - 1));

        switch (rnd) {
            case 1:
                txt = "There have been reports of increased space pirate activity around " + planet + ". The government has issued a travel alert.";
                break;
            case 2:
                txt = "The government of " + planet + " has lifted the travel alert to the planet. It's now safe again to fly around the sector.";
                break;
            case 3:
                txt = "Due to the recent border skirmishes around " + planet + ", the government has issued a temporary trading ban there.";
                break;
            case 4:
                txt = faction + " have increased their control on the markets of " + planet + ".";
                break;
            case 5:
                txt = "The government of " + planet + " is cracking down on the ownership of yellowberries. Opposition appalled.";
                break;
            case 6:
                txt = "The government of " + planet + " is advising against reversing the polarity at home.";
                break;
            case 7:
                txt = "Foreign exchanges are reporting major losses all over the board.";
                break;
            case 8:
                txt = "Subordinate-rated transfers are to be insulated in the agricultural sector. Farmers are in uproar.";
                break;
            case 9:
                txt = "Recent Low-IRR buyouts have angered the crabmen of Neptulon. " + faction + " are promising corrections.";
                break;
            case 10:
                txt = "Liquid liability structures in the financial market are expected to be undermined by promiscuous tree ants. Government confused.";
                break;
            case 11:
                txt = "The government of " + planet + " is planning to undermine foreign iglo-investments. Market rating has been lowered.";
                break;
            case 12:
                txt = "Traders are encouraged to leverage their revenue-neutral derivatives. Investors are crippled.";
                break;
            case 13:
                txt = faction + " are encouraging traders by lowering interest rates.";
                break;
            case 14:
                txt = "Accusations of industrial sabotage lower trust in " + faction + ". Investors are alarmed.";
                break;
            case 15:
                txt = "Evidence of market manipulation have lowered confidence in the " + planet + " government.";
                break;
            case 16:
                txt = faction + " share their metallurgy secrets, dazzling the public.";
                break;
            case 17:
                txt = "Unsecured asset pools are being redistributed. Underground caverns will be shut down until further notice.";
                break;
            case 18:
                txt = "Latest tests in the field of liquid spectromagrophy seem promising. Investors are buzzing.";
                break;
            case 19:
                txt = "Rumors of an impending war between " + planet + " and " + faction + " leave traders alarmed.";
                break;
            case 20:
                txt = "Buy low! Sell high!";
                break;
            case 21:
                txt = "Aubergines are rumoured to be fake. Public alarmed.";
                break;
            case 22:
                txt = "Royal guard raids weapon shop, find weapons.";
                break;
            case 23:
                txt = "Amphibious cows are losing their jobs. Traders are in uproar!";
                break;
            case 24:
                txt = "Miracle cure turns out to be deadly.";
                break;
            case 25:
                txt = "A mysterious case of the Cargulian space-worm has been spotted in stores all over the city.";
                break;
            case 26:
                txt = "Newly born babies are expected to cause a rise in population numbers.";
                break;
            case 27:
                txt = "Voters to vote on whether to vote.";
                break;
            case 28:
                txt = "A massive influx of financially unstable goatmen is causing tensions between members of " + faction + ".";
                break;
        }

        return paper + "\nToday's headline : \n\n" + txt;

    }

    private String weather() {

        int rnd = MathUtils.random(1, 10);
        String txt = "";
        String planet = player.getCurrentPlanet().getPlanetName();

        switch (rnd) {
            case 1:
                txt = "Looks like it's going to be a hot day today with temperatures going well above the average for this time of year. " +
                        "Some light drizzle is expected later this afternoon so don't forget your umbrella!";
                break;
            case 2:
                txt = "We're expecting a lot of of hotness and humidity today, so don't forget to dress light! " +
                        "The clouds may clear up later today, causing it to be lot less humid.";
                break;
            case 3:
                txt = "Today will be mostly cloudy with a small chance of rain. The skies will have cleared up before nightfall, " +
                        "causing a major drop in temperature.";
                break;
            case 4:
                txt = "A high pressure front will bring dry weather today. It will be mostly cloudy by tonight, " +
                        "but not as cool as the last couple of days.";
                break;
            case 5:
                txt = "Some showers and a few thunderstorms will be possible this afternoon into tomorrow evening " +
                        "when a cold front is forecast to drop through the region.";
                break;
            case 6:
                txt = "It's going to be a beautiful day today, depending on your personal definition of the word. We can expect " +
                        "drier air later this evening with some light winds.";
                break;
            case 7:
                txt = "We're expecting some pleasant weather throughout the rest of the day. " +
                        "Some rain is likely by the end of tomorrow.";
                break;
            case 8:
                txt = "Temperatures will fall rapidly tomorrow and winds will shift from the northwest, making it feel a lot cooler than the first part of the week.";
                break;
            case 9:
                txt = "Heavy snowfall is expected today and tomorrow. Temperatures will stay in the lows for the coming days.";
                break;
            case 10:
                txt = "Some icy cold weather is coming our way this week. The day starts of with a heavy hailstorm so dress appropriately.";
                break;
        }

        return "Welcome to " + planet + "!\n\n" + txt;

    }

    private String randomSurplusText(Commodity c) {

        int rnd = MathUtils.random(1, 9);
        String txt = "";

        switch (rnd) {
            case 1:
                txt = "Because of a sudden influx of migrating crabmen, the market is flooded with cheap " +
                        c.getCommodityName() + ".\n\nGet some while you can!";
                break;
            case 2:
                txt = "The government is cracking down on counterfeit " +
                        c.getCommodityName() + ".\n\nPrices are dropping fast!";
                break;
            case 3:
                txt = "A anonymous seller has dropped a large amount of " + c.getCommodityName() + " on the market. \n\n" +
                        "Now is the time to buy!";
                break;
            case 4:
                txt = "As a gesture of good will, " + player.getCurrentPlanet().getFactionName() + " are donating their surplus of " +
                        c.getCommodityName() + " to the people.\n\nPrices have dropped greatly!";
                break;
            case 5:
                txt = "An overproduction of " + c.getCommodityName() + " have caused the market to be flooded.\n\nPrices have dropped, it's time to buy!";
                break;
            case 6:
                txt = "Consumers are losing faith in the effectiveness of " + c.getCommodityName() + ".\n\nMarkets are saturated." +
                        " Now is a good time to buy!";
                break;
            case 7:
                txt = "The recent discovery of a massive copper deposit on one of the moons of " + player.getCurrentPlanet().getPlanetName() +
                        " is causing consumers to dump their " + c.getCommodityName() + " in great numbers.\n\nMarkets are saturated, now would be good time to buy.";
                break;
            case 8:
                txt = player.getCurrentPlanet().getFactionName() + " are publicly pulling their support for producers of " + c.getCommodityName() +
                        ". \n\nPrices are dropping fast!";
                break;
            case 9:
                txt = "The price of " + c.getCommodityName() + " is rapidly falling. Consumer organisations are launching an inquiry.\n\n" +
                        "In the meantime, now would be a good time to buy.";
                break;
        }

        return txt;
    }

    private String randomShortageText(Commodity c) {

        int rnd = MathUtils.random(1, 7);
        String txt = "";

        switch (rnd) {
            case 1:
                txt = "A mysterious buyer has bought up most of the " + c.getCommodityName() + " on " + player.getCurrentPlanet().getPlanetName() + "." +
                        "\n\nPrices have skyrocketed!";
                break;
            case 2:
                txt = "Because of ongoing efforts against the proliferation of " + c.getCommodityName() + " by " + player.getCurrentPlanet().getFactionName() +
                        ", prices are rising considerably.\n\nTime to dump any surplus of " + c.getCommodityName() + "!";
                break;
            case 3:
                txt = "Migrating crabmen are eating up all of the " + c.getCommodityName() + ". There's hardly any left on the markets." +
                        "\n\nPrices are soaring!";
                break;
            case 4:
                txt = "The demand for " + c.getCommodityName() + " is rising rapidly because of newly discovered health benefits. \n\nBuyers are clearing out the markets" +
                        ", prices are high!";
                break;
            case 5:
                txt = "Because of renewed terraforming efforts of " + player.getCurrentPlanet().getFactionName() + ", the demand for " +
                        c.getCommodityName() + " is rising rapidly.\n\nNow would be a good time to sell!";
                break;
            case 6:
                txt = "The discovery of a healthier use for " + c.getCommodityName() + " has increased its demand.\n\nTime to sell any stock you have!";
                break;
            case 7:
                txt = player.getCurrentPlanet().getFactionName() + " are buying up all of the " + c.getCommodityName() + " for unknown reasons." +
                        "\n\nMajor shortages reported!";
                break;
        }

        return txt;
    }
}