package game.peanutpanda.spacetrade.random;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.gameobjects.Commodity;


public class Randomiser {

    int c1Lower, c1Upper, c2Lower, c2Upper, c3Lower, c3Upper, c4Lower, c4Upper, c5Lower, c5Upper,
            c6Lower, c6Upper, c7Lower, c7Upper, c8Lower, c8Upper, c9Lower, c9Upper, c10Lower, c10Upper;

    private Array<Integer> lowPrices;
    private Array<Integer> highPrices;
    private int amountLower;
    private int amountUpper;

    public Randomiser() {

        lowPrices = new Array<>();
        highPrices = new Array<>();

        c1Lower = 100;
        c1Upper = 200;

        c2Lower = 100;
        c2Upper = 300;

        c3Lower = 100;
        c3Upper = 400;

        c4Lower = 150;
        c4Upper = 500;

        c5Lower = 300;
        c5Upper = 700;

        c6Lower = 300;
        c6Upper = 900;

        c7Lower = 500;
        c7Upper = 1000;

        c8Lower = 800;
        c8Upper = 1500;

        c9Lower = 1000;
        c9Upper = 2000;

        c10Lower = 2000;
        c10Upper = 3000;

        amountLower = 0;
        amountUpper = 240;

        lowPrices.add(c1Lower);
        lowPrices.add(c2Lower);
        lowPrices.add(c3Lower);
        lowPrices.add(c4Lower);
        lowPrices.add(c5Lower);
        lowPrices.add(c6Lower);
        lowPrices.add(c7Lower);
        lowPrices.add(c8Lower);
        lowPrices.add(c9Lower);
        lowPrices.add(c10Lower);

        highPrices.add(c1Upper);
        highPrices.add(c2Upper);
        highPrices.add(c3Upper);
        highPrices.add(c4Upper);
        highPrices.add(c5Upper);
        highPrices.add(c6Upper);
        highPrices.add(c7Upper);
        highPrices.add(c8Upper);
        highPrices.add(c9Upper);
        highPrices.add(c10Upper);

    }

    public void randomiseCommodities(Array<Commodity> commodities) {
        randomiseCommoditiesAmount(commodities);
        randomiseCommoditiesPrice(commodities);
    }

    private int lowestAmount() {
        return amountUpper / 6;
    }

    private int highestAmount() {
        return amountUpper - (amountUpper / 6);
    }

    private int middleAmount() {
        return (amountUpper / 6) * 3;
    }

    private boolean supplyIsMiddleLow(Commodity commodity) {
        return commodity.getAmountOnPlanet() <= middleAmount() && commodity.getAmountOnPlanet() > lowestAmount();
    }

    private boolean supplyIsMiddleHigh(Commodity commodity) {
        return commodity.getAmountOnPlanet() > middleAmount() && commodity.getAmountOnPlanet() < highestAmount();
    }

    private boolean supplyIsLowest(Commodity commodity) {
        return commodity.getAmountOnPlanet() <= lowestAmount();
    }

    private boolean supplyIsHighest(Commodity commodity) {
        return commodity.getAmountOnPlanet() >= highestAmount();
    }

    private void setHighMiddlePrice(Commodity commodity, int i) {
        commodity.setPrice(MathUtils.random(highPrices.get(i) - highPrices.get(i) / 4, highPrices.get(i) - highPrices.get(i) / 10));
    }

    private void setLowMiddlePrice(Commodity commodity, int i) {
        commodity.setPrice(MathUtils.random(lowPrices.get(i) + highPrices.get(i) / 10, lowPrices.get(i) + highPrices.get(i) / 4));
    }

    private void setHighestPrice(Commodity commodity, int i) {
        commodity.setPrice(MathUtils.random(highPrices.get(i) - highPrices.get(i) / 10, highPrices.get(i)));
    }

    private void setLowestPrice(Commodity commodity, int i) {
        commodity.setPrice(MathUtils.random(lowPrices.get(i), lowPrices.get(i) + highPrices.get(i) / 10));
    }

    private void setRandomPrice(Commodity commodity, int i) {
        commodity.setPrice(MathUtils.random(lowPrices.get(i), highPrices.get(i)));
    }

    private void randomiseCommoditiesPrice(Array<Commodity> commodities) {

        for (int i = 0; i < commodities.size; i++) {

            if (supplyIsLowest(commodities.get(i))) {
                setHighestPrice(commodities.get(i), i);

            } else if (supplyIsMiddleLow(commodities.get(i))) {
                setHighMiddlePrice(commodities.get(i), i);

            } else if (supplyIsMiddleHigh(commodities.get(i))) {
                setLowMiddlePrice(commodities.get(i), i);

            } else if (supplyIsHighest(commodities.get(i))) {
                setLowestPrice(commodities.get(i), i);

            } else {
                setRandomPrice(commodities.get(i), i);
            }
        }
    }

    private void randomiseCommoditiesAmount(Array<Commodity> commodities) {
        commodities.get(0).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(1).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(2).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(3).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(4).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(5).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(6).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(7).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(8).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));
        commodities.get(9).setAmountOnPlanet(MathUtils.random(amountLower, amountUpper));

    }
}
