package game.peanutpanda.spacetrade.random;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.Commodity;
import game.peanutpanda.spacetrade.gameobjects.Player;
import game.peanutpanda.spacetrade.gameobjects.TaxUrgency;
import game.peanutpanda.spacetrade.gameobjects.Vars;

import java.text.NumberFormat;
import java.util.Locale;

public class TravelEvent {

    public Vars vars;
    public Player player;
    public SpaceTrade game;
    public boolean doesNotQualify;

    public String acceptResult, yesButtonText, noButtonText, okButtonText;
    public int choice;
    public long gambleMoney;
    int rndPassenger, rndFreeCargo;
    String planet;
    Commodity freeCargo;
    long marketeerPrice;
    String marketeerName;
    String cheapCommodityName;
    int cheapCommAmount;
    int cheapCommTotalCost;
    Commodity cheapCommodity;
    String exchangeCargoName;

    public boolean fined;

    public TravelEvent(Vars vars, Player player, SpaceTrade game) {
        this.vars = vars;
        this.player = player;
        this.game = game;
    }

    public String taxWarning() {
        doesNotQualify = true;
        player.isWarnedAboutTaxes = true;
        String txt = "You receive an urgent message from the Interplanetary Revenue Service...\n\n";

        txt += "\"According to our infallible records, we have determined that you have unpaid taxes that are urgently due." +
                "This message is your notice of deficiency, as required by law. Please take care of this immediately to " +
                "avoid possible penalties.";

        return txt + "\n\nYou have 4 weeks to deliver the unpaid amount to the Imperial Seat.\"";
    }

    public String taxPenalty() {
        doesNotQualify = true;
        planet = player.getCurrentPlanet().getPlanetName();
        player.isWarnedAboutTaxes = false;
        player.setTaxUrgencyCounter(0);
        long money = player.getMoney() / 5;
        String fine = NumberFormat.getNumberInstance(Locale.US).format(money);
        String txt = "You receive an urgent message from the Interplanetary Revenue Service...\n\n";

        player.setTaxRate(player.getTaxRate() + 2);
        fined = true;

        player.subtractMoney(money);
        for (Commodity commodity : vars.commodities) {
            commodity.setAmountOnShip(0);
        }

        player.cargoOnBoard = 0;

        txt += "\"Due to your unwillingness to pay your taxes, the Imperial Seat seizes all of your goods and donates them to the Imperial Seat.\n\n" +
                "You are also fined to the amount of " + fine + " credits, to be paid immediately.\"\n\n" +
                "You notice the Imperial Seat has also increased your tax rate by 2 percent.";

        return txt;
    }

    public String randomEventIntroText(TaxUrgency taxUrgency) {

        doesNotQualify = false;
        planet = player.getCurrentPlanet().getPlanetName();
        int rnd = MathUtils.random(1, 14);
        int rndTax = MathUtils.random(1, 3);
        String txt = "";

        System.out.println(rnd);

        if (player.getTaxUrgencyCounter() > 3) {
            txt = taxPenalty();

        } else if (!player.isWarnedAboutTaxes && taxUrgency == TaxUrgency.CRITICAL && rndTax > 1) {
            txt = taxWarning();

        } else {

            yesButtonText = "Accept";
            noButtonText = "Decline";
            gambleMoney = player.getMoney() / 2;

            switch (rnd) {
                case 1:
                    yesButtonText = "Let her in";
                    noButtonText = "Leave her be";
                    txt = "On your way to " + planet + ", you are hailed by a passing ship." +
                            "A woman inside claims to be the princess of " + planet + " and is late for her birthday party.\n\n" +
                            "Looks like she started celebrating early. She drunkenly asks you to bring her to her party on" +
                            " time and promises to reward you with something special upon arrival.";
                    break;
                case 2:
                    yesButtonText = "Let him in";
                    noButtonText = "Leave him be";

                    txt = "Halfway through the trip, you come across a young hitchhiker. He says he's travelling the system and wants to visit every planet there is." +
                            "He notices you're on your way to " + planet + " which happens to be the next planet on his list.\n\n" +
                            "He humbly asks if you could give him a lift.";
                    break;
                case 3:
                    yesButtonText = "Accept";
                    noButtonText = "Decline";

                    txt = "Halfway through the trip, someone flags you down. A man humbly asks you if you would be so kind as to " +
                            "give his dog a ride to " + planet + ".\n\n" +
                            "He tells you the dogs' name is Flumpfer and an adoption family is waiting for him there." +
                            "\n\nHe'd do it himself but he was unexpectedly called away on urgent family business";
                    break;
                case 4:
                case 5:
                case 6:
                    rndPassenger = MathUtils.random(1, 6);
                    String intro = "On the way to " + planet + ", you receive a broadcast...\n\n";

                    switch (rndPassenger) {
                        case 1:
                            yesButtonText = "Let her in";
                            noButtonText = "Leave her be";
                            txt = intro + "A travelling merchant is looking for a ride as her previous one bailed on her. She says she would be eternally grateful if you" +
                                    " decide to take her with you to " + planet + ".";
                            break;
                        case 2:
                            yesButtonText = "Let him in";
                            noButtonText = "Leave him be";
                            txt = intro + "A young minstrel asks you for a ride. He says he will thank by singing you songs of the ancient world.";
                            break;
                        case 3:
                            yesButtonText = "Let him in";
                            noButtonText = "Leave him be";
                            txt = intro + "An elite hacker wants a ride on your ship. He promises to upgrade your navigational software as a reward if you take him all " +
                                    "the way to " + planet + ".";
                            break;
                        case 4:
                            yesButtonText = "Let him in";
                            noButtonText = "Leave him be";
                            txt = intro + "A young man identifying himself as the Dark Lord demands a ride to " + planet + ", which happens to be your next destination.";
                            break;

                        case 5:
                            yesButtonText = "Let them in";
                            noButtonText = "Leave them be";
                            txt = intro + "A family of four requests a ride to " + planet + ". They can't pay you but they hope you can enjoy their company.";
                            break;

                        case 6:
                            yesButtonText = "Let them in";
                            noButtonText = "Leave them be";
                            txt = intro + "A team of professional cleaners request transit to " + planet + ". They don't have much money but " +
                                    "they will thoroughly clean your ship as a way to repay your kindness.";
                            break;
                    }
                    break;
                case 7:
                case 8:
                    int rndFreeC = MathUtils.random(1, 2);

                    if (!player.hasRoomLeft()) {
                        doesNotQualify = true;
                        Commodity c = vars.commodities.get(MathUtils.random(0, vars.commodities.size - 1));

                        switch (rndFreeC) {
                            case 1:
                                txt = "A passing ship hails you. Its pilot tells you he urgently has to get rid of these " + c.getCommodityName() + " he's carrying. " +
                                        "He offers them all to you, free of charge.\n\n" +
                                        "Too bad your cargo hold is already filled to the brim. You thank the pilot and continue your journey.";
                                break;
                            case 2:
                                txt = "You come across a long abandoned ship. Upon closer inspection, you find out that the vessel has a bunch of " + c.getCommodityName() + "!" +
                                        "\n\nNice find! Too bad your cargo hold is already filled to the brim. You leave the goods behind and continue your journey.";
                                break;
                        }

                    } else {
                        yesButtonText = "Take it";
                        noButtonText = "Leave it";
                        freeCargo = vars.commodities.get(MathUtils.random(0, vars.commodities.size - 1));
                        int rndIntro = MathUtils.random(1, 3);
                        rndFreeCargo = MathUtils.random(1, 2);

                        switch (rndFreeCargo) {
                            case 1:
                                switch (rndIntro) {
                                    case 1:
                                        txt = "A passing ship hails you. Its pilot tells you the " + freeCargo.getCommodityName() + " he's carrying will go bad before he reaches his destination. " +
                                                "\n\nHe offers them all to you, free of charge.";
                                        break;
                                    case 2:
                                        txt = "A passing ship hails you. Its pilot tells you he urgently has to get rid of these " + freeCargo.getCommodityName() + " he's carrying. " +
                                                "\n\nHe offers them all to you, free of charge.";
                                        break;
                                    case 3:
                                        txt = "A passing ship hails you. Its pilot offers you all of the " + freeCargo.getCommodityName() + " he's carrying. He tells he urgently has to tend to " +
                                                "a family emergency and this stuff is only slowing him down. " +
                                                "\n\nHe offers them all to you, free of charge.";
                                        break;
                                }
                                break;
                            case 2:
                                switch (rndIntro) {
                                    case 1:
                                        txt = "You come across a long abandoned ship. Upon closer inspection, you find out that the vessel has a bunch of " + freeCargo.getCommodityName() +
                                                " still in his cargo hold.\n\nSeems like quite the free prize! You can take it or leave it.";
                                        break;
                                    case 2:
                                        txt = "You come across a derelict ship. It looks very old and was probably abandoned a long time ago." +
                                                " Its cargo hold still seems to have a batch of " + freeCargo.getCommodityName() + " in it!" +
                                                "\n\nSeems like quite the free prize! You can take it or leave it.";
                                        break;
                                    case 3:
                                        txt = "Your scanners pick up some abandoned containers drifting through space." +
                                                " They seem to be holding a lot of " + freeCargo.getCommodityName() + "!" +
                                                "\n\nLooks like quite the free prize! You can take it or leave it.";
                                        break;
                                }

                                break;
                        }
                    }


                    break;
                case 9:
                    txt = "The pilot from a passing ship requests entry, claims he has an interesting proposition for you. Intrigued, you let him in." +
                            "\n\n\"Hi there, care to place a small wager?\" the man asks, " +
                            "\"Here's the deal, I throw this die. Heads, you give me half of your credits." +
                            " Tails, I'll pay you that amount right now. Interested?\"\n\n" +
                            "The amount of this bet is worth " + NumberFormat.getNumberInstance(Locale.US).format(gambleMoney) + " credits.";
                    break;
                case 10:
                case 11:
                    Array<String> names = new Array<>();
                    names.addAll("Mister Gringzeb", "Mister Kloko", "Mister Fab", "Mister Gojo", "Mister Timbok", "Mister Yurt", "Mister Ochimo");
                    marketeerName = names.get(MathUtils.random(0, names.size - 1));

                    if (player.getMoney() < 20000) {
                        doesNotQualify = true;
                        txt = "You are contacted by " + marketeerName + ", the famous marketeer.\n\nHe has an interesting proposition for you. " +
                                "He says he has the power to influence the markets upon your arrival on " + player.getCurrentPlanet().getPlanetName() + ".\n\n" +
                                marketeerName + " can provide his services for " + NumberFormat.getNumberInstance(Locale.US).format(20000) + " credits.\n\n" +
                                "Too bad you don't have enough money. You thank the marketeer and continue your journey.";

                    } else {
                        marketeerPrice = player.getMoney() < 120000 ? player.getMoney() / 4 : MathUtils.random(50000, 150000);
                        txt = "You are contacted by " + marketeerName + ", the famous marketeer.\n\nHe has an interesting proposition for you. " +
                                "He says he has the power to influence the markets upon your arrival on " + player.getCurrentPlanet().getPlanetName() + ". " +
                                "He claims that after his intervention, the prices will be very favourable for you.\n\n" +
                                marketeerName + " can provide his services for " + NumberFormat.getNumberInstance(Locale.US).format(marketeerPrice) + " credits.";
                    }
                    break;
                case 12:
                case 13:
                    Array<String> namesCheapComm = new Array<>();
                    namesCheapComm.addAll("Yamuzi", "Chinfu", "Davo", "Miss Yun", "Lady Kizona", "Lady Iopila", "Miss Riukiu", "Fruti");
                    cheapCommodityName = namesCheapComm.get(MathUtils.random(0, namesCheapComm.size - 1));

                    cheapCommodity = vars.commodities.get(MathUtils.random(0, vars.commodities.size - 1));
                    int cPrice = cheapCommodity.getPrice() / 4;
                    cheapCommAmount = player.getCargoHoldSize() - player.getCargoOnBoard();
                    cheapCommTotalCost = cPrice * cheapCommAmount;

                    if (!player.hasRoomLeft()) {
                        doesNotQualify = true;
                        txt = "A voice calls you over the radio, says her name is " + cheapCommodityName + " and she claims she has a good deal for you!" +
                                "\n\nShe wants to sell you these " + cheapCommodity.getCommodityName() + " for a quarter of the price they're worth.\n\n" +
                                "Too bad your cargo hold is already filled to the brim. You thank " + cheapCommodityName + " and continue your journey.";

                    } else if (player.getMoney() < cheapCommTotalCost) {
                        doesNotQualify = true;
                        txt = "A voice calls you over the radio, says her name is " + cheapCommodityName + " and she claims she has a good deal for you!" +
                                "\n\nShe wants to sell you these " + cheapCommodity.getCommodityName() + " for a quarter of the price they're worth.\n\n" +
                                "This transaction will cost you " + NumberFormat.getNumberInstance(Locale.US).format(cheapCommTotalCost) + " credits. Too bad you don't have that kind of money.\n\n" +
                                "You thank " + cheapCommodityName + " and continue your journey.";

                    } else {
                        txt = "A voice calls you over the radio, says her name is " + cheapCommodityName + " and she claims she has a good deal for you!" +
                                "\n\nShe wants to sell you " + NumberFormat.getNumberInstance(Locale.US).format(cheapCommAmount) + " " + cheapCommodity.getCommodityName() + " for a quarter of the price" +
                                " they're usually worth.\n\n" +
                                "This transaction will cost you " + NumberFormat.getNumberInstance(Locale.US).format(cheapCommTotalCost) + " credits.";
                    }
                    break;
                case 14:
                    Array<String> namesExchangeCargo = new Array<>();
                    namesExchangeCargo.addAll("Mister Sniffo", "Mister Yappers", "Brinto", "Stampy", "Yilbaz", "Mister Snapper", "Mister Yukaizo");
                    exchangeCargoName = namesExchangeCargo.get(MathUtils.random(0, namesExchangeCargo.size - 1));

                    if (player.cargoOnBoard == 0) {
                        doesNotQualify = true;
                        txt = "You are contacted by " + exchangeCargoName + ", the wealthy industrialist.\n\nHe tells you he is looking to make a deal, but after noticing" +
                                " you are trucking around with an empty cargo hold he decides to take his business elsewhere.\n\n" +
                                "\"Maybe some other time!\" he says while he zips away.";

                    } else {
                        Commodity c = vars.commodities.get(9);
                        txt = "You are contacted by " + exchangeCargoName + ", the wealthy industrialist.\n\nHe has an interesting proposition for you.\n\n" +
                                exchangeCargoName + " wants to exchange everything you are carrying in your cargo hold for an equal amount of highly valued " + c.getCommodityName() + " " +
                                "for no extra cost.\n\n" +
                                "He promises you that this the deal of a lifetime.";
                    }
                    break;
            }
        }

        choice = rnd;
        return txt;

    }

    public void eventAcceptedResult() {

        switch (choice) {
            case 1:
                princessPassenger();
                break;
            case 2:
                hitchHikerPassenger();
                break;
            case 3:
                petPassenger();
                break;
            case 4:
            case 5:
            case 6:
                randomHitchhikers(rndPassenger, planet);
                break;
            case 7:
            case 8:
                int rndResult = MathUtils.random(1, 6);
                int amountFreeCargo = player.getCargoHoldSize() - player.getCargoOnBoard();
                long money;
                switch (rndFreeCargo) {
                    case 1:
                        switch (rndResult) {
                            case 1:
                            case 2:
                            case 3:
                                player.cargoOnBoard += amountFreeCargo;
                                freeCargo.transferToShipRandomEvent(amountFreeCargo);
                                freeCargo.addToBoughtAtPrice(1, amountFreeCargo);
                                acceptResult = "The pilot transfers " + NumberFormat.getNumberInstance(Locale.US).format(amountFreeCargo) + "" +
                                        " " + freeCargo.getCommodityName() + " to your cargo, free of charge.";
                                okButtonText = "Nice!";
                                break;

                            case 4:
                                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                                player.subtractMoney(money);
                                acceptResult = "The pilot transfers " + NumberFormat.getNumberInstance(Locale.US).format(amountFreeCargo) + " " + freeCargo.getCommodityName() + "" +
                                        " to your cargo. Before you arrive at " + player.getCurrentPlanet().getPlanetName() + ", you notice a strange smell" +
                                        " coming out of your cargo hold. It seems that the " + freeCargo.getCommodityName() + " were malfunctioning and caused a small fire" +
                                        ", destroying all of the " + freeCargo.getCommodityName() + ".\n\n" +
                                        "This results in " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits worth of damages.";
                                okButtonText = "Bummer!";
                                break;
                            case 5:
                                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                                player.subtractMoney(money);
                                acceptResult = "The pilot transfers " + NumberFormat.getNumberInstance(Locale.US).format(amountFreeCargo) + " " + freeCargo.getCommodityName() + "" +
                                        " to your cargo. On your way to " + player.getCurrentPlanet().getPlanetName() + ", you notice a strang sound" +
                                        " coming out of your cargo hold. It seems that the " + freeCargo.getCommodityName() + " are infested with Mangrulian toe-lice!.\n\n" +
                                        "Getting rid of them costs you " + NumberFormat.getNumberInstance(Locale.US).format(money) + " and " +
                                        "the loss of all of the " + freeCargo.getCommodityName() + ".";
                                okButtonText = "Bummer!";
                                break;
                            case 6:
                                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                                player.subtractMoney(money);
                                acceptResult = "The pilot transfers " + NumberFormat.getNumberInstance(Locale.US).format(amountFreeCargo) + " " + freeCargo.getCommodityName() + "" +
                                        " to your cargo. On your way to " + player.getCurrentPlanet().getPlanetName() + ", three ships pull up behind you. Turns out " +
                                        "it's space pirates! After talking to them for a while you manage to convince them not to take your ship. " +
                                        "They still leave with all of your " + freeCargo.getCommodityName() + ". As space pirates are not known for their finesse, they also " +
                                        "manage to cause some damage to your ship while stealing your cargo.\n\nThe damage is estimated" +
                                        " at " + NumberFormat.getNumberInstance(Locale.US).format(money) + ".";
                                okButtonText = "Bummer!";
                                break;
                        }
                        break;
                    case 2:
                        int rndResult2 = MathUtils.random(1, 6);
                        switch (rndResult2) {
                            case 1:
                            case 2:
                            case 3:
                                player.cargoOnBoard += amountFreeCargo;
                                freeCargo.transferToShipRandomEvent(amountFreeCargo);
                                acceptResult = "You fill up your cargo hold with as many of the " + freeCargo.getCommodityName() + " it can hold.\n\n" +
                                        "Great find!";
                                okButtonText = "Nice!";
                                break;
                            case 4:
                                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                                player.subtractMoney(money);
                                acceptResult = "You fill up your cargo hold with as many of the " + freeCargo.getCommodityName() + " it can hold.\n\n" +
                                        "As you leave, two ships pull up in front of you. It's the space police!\n\n" +
                                        "Too bad! You receive a fine because of salvaging without a valid permit. " +
                                        "\n\nThe space police takes " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits out of your account " +
                                        "and confiscates the salvaged cargo.";
                                okButtonText = "Ouch!";
                                break;
                            case 5:
                                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                                player.subtractMoney(money);
                                acceptResult = "You fill up your cargo hold with as many of the " + freeCargo.getCommodityName() + " it can hold.\n\n" +
                                        "As you leave, two ships pull up in front of you. Space pirates!\n\n" +
                                        "Too bad! Looks like they've been following you for quite a while. " +
                                        "\n\nThe space pirates take " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits out of your account " +
                                        "and take away the salvaged cargo.";
                                okButtonText = "Ouch!";
                                break;
                            case 6:
                                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                                player.subtractMoney(money);
                                acceptResult = "You fill up your cargo hold with as many of the " + freeCargo.getCommodityName() + " it can hold.\n\n" +
                                        "Right as you leave, the found cargo causes an explosion in your cargo hold. " +
                                        "Looks like the goods were booby trapped!" +
                                        "\n\nAll of the salvaged cargo is destroyed and the repair costs are" +
                                        " estimated at " + NumberFormat.getNumberInstance(Locale.US).format(money) + ".";
                                okButtonText = "Ouch!";
                                break;
                        }
                        break;
                }
                break;
            case 9:
                gambleDice(gambleMoney);
                break;
            case 10:
            case 11:
                marketeer(marketeerName);
                break;
            case 12:
            case 13:
                cheapCommodity(cheapCommAmount, cheapCommodity, cheapCommTotalCost, cheapCommodityName);
                break;
            case 14:
                cargoExchange(exchangeCargoName);
                break;
        }
    }

    public void gambleDice(long money) {

        int rndResult = MathUtils.random(1, 2);

        switch (rndResult) {
            case 1:
                player.subtractMoney(money);
                acceptResult = "Heads! You lose!\n\n\"Too bad!\" says the man, \"Better luck next time!\"\n\n" +
                        "He leaves your ship with half of your money.";
                okButtonText = "Ouch!";
                break;
            case 2:
                player.addMoney(money);
                acceptResult = "Tails! You win!\n\n\"Ouch, that hurts!\" says the man, \"I guess it's not my lucky day.\"\n\n" +
                        "Before he leaves your ship, he credits your account with " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits.";
                okButtonText = "Nice!";
                break;
        }

    }


    private void cheapCommodity(int amount, Commodity c, int totalCost, String name) {

        int rndResult = MathUtils.random(1, 3);

        switch (rndResult) {
            case 1:
                player.cargoOnBoard += amount;
                c.transferToShipRandomEvent(amount);
                c.addToBoughtAtPrice(1, amount);
                player.subtractMoney(totalCost);
                acceptResult = "You buy " + NumberFormat.getNumberInstance(Locale.US).format(amount) + " " + c.getCommodityName() + " for just a fraction " +
                        "of the price they're usually worth.\n\nLooks like you made a great deal today!";
                okButtonText = "Nice!";
                break;
            case 2:
                player.subtractMoney(totalCost);
                acceptResult = "You transfer " + NumberFormat.getNumberInstance(Locale.US).format(totalCost) + " credits to the account" +
                        " of " + name + ".\n\n\"Thanks! I'll send you the goods in a couple of weeks. Take care!\"\n\n" +
                        "You hope " + name + " will keep her word.";
                okButtonText = "Continue";
                break;
            case 3:
                player.cargoOnBoard += amount / 2;
                player.subtractMoney(totalCost);
                c.transferToShipRandomEvent(amount / 2);
                c.addToBoughtAtPrice(1, amount);
                acceptResult = "You transfer " + NumberFormat.getNumberInstance(Locale.US).format(totalCost) + " to the account" +
                        " of " + name + ".\n\n\"Thanks! Here are the goods. Take care!\"\n\n" +
                        "As you inspect your newly gained cargo, you notice that a large part of it has been infected by a case of the Cargulian space-worms!\n" +
                        "\n\nYou manage to keep some of it but you are forced to throw half of it away.";
                okButtonText = "Continue";
                break;
        }

    }


    private void lowerAllPrices() {
        for (Commodity c : vars.commodities) {
            c.setPrice(c.getPrice() / 4);
        }
    }

    private void increaseAllPrices() {
        for (Commodity c : vars.commodities) {
            c.setPrice(c.getPrice() * 2);
        }
    }

    public void cargoExchange(String name) {

        int amount = player.cargoOnBoard;
        Commodity c = vars.commodities.get(9);

        int rndResult = MathUtils.random(1, 4);

        switch (rndResult) {
            case 1:
            case 2:
                for (Commodity commodity : vars.commodities) {
                    commodity.setAmountOnShip(0);
                }
                player.cargoOnBoard = 0;
                c.transferToShipRandomEvent(amount);
                player.cargoOnBoard += amount;
                c.addToBoughtAtPrice(1, amount);
                acceptResult = name + " transfers " + NumberFormat.getNumberInstance(Locale.US).format(amount) + " of " + c.getCommodityName() + " to your hold and takes what's in there for himself, as " +
                        "stipulated in your agreement. \n\n" +
                        "You're sure to make some great profits on these valuable " + c.getCommodityName() + "!";
                okButtonText = "Nice!";
                break;

            case 3:
                for (Commodity commodity : vars.commodities) {
                    commodity.setAmountOnShip(0);
                }
                Commodity wrongCommodity = vars.commodities.get(MathUtils.random(2, 6));
                player.cargoOnBoard = 0;
                wrongCommodity.transferToShipRandomEvent(amount);
                player.cargoOnBoard += amount;
                wrongCommodity.addToBoughtAtPrice(1, amount);
                acceptResult = name + " transfers " + NumberFormat.getNumberInstance(Locale.US).format(amount) + " of " + c.getCommodityName() + " to your hold and takes what's in there for himself, as " +
                        "stipulated in your agreement. \n\n" +
                        "As he leaves again, you decide to take a look at your newly gained goods. Upon closer inspection it looks like these are not at all the" +
                        " " + c.getCommodityName() + " as " +
                        exchangeCargoName + " promised you, but instead he's given you " + NumberFormat.getNumberInstance(Locale.US).format(amount) + " " + wrongCommodity.getCommodityName() + "!" +
                        "\n\nYou're not sure if this was an honest mistake or that you've been swindled.";
                okButtonText = "Hmmmm";
                break;

            case 4:
                for (Commodity commodity : vars.commodities) {
                    commodity.setAmountOnShip(0);
                }
                player.cargoOnBoard = 0;
                acceptResult = name + " transfers " + NumberFormat.getNumberInstance(Locale.US).format(amount) + " of " + c.getCommodityName() + " to your hold and takes what's in there for himself, as " +
                        "stipulated in your agreement. \n\n" +
                        "As you attempt to leave, two ships pull up in front of you. It's the space police!\n\n" +
                        "Turns out " + exchangeCargoName + " is a known smuggler and wanted by the law. The space police confiscates all of your cargo.";
                okButtonText = "Ouch!";
                break;
        }

    }

    public void marketeer(String name) {

        int rndResult = MathUtils.random(1, 3);

        switch (rndResult) {
            case 1:
                player.subtractMoney(marketeerPrice);
                lowerAllPrices();
                acceptResult = name + " is a marketeer of his word. Upon your arrival on " + player.getCurrentPlanet().getPlanetName() + ", " +
                        "you notice that prices have dropped considerably.\n\n" +
                        "Time to make some great deals!";
                okButtonText = "Nice!";
                break;

            case 2:
                player.subtractMoney(marketeerPrice);
                acceptResult = "As you land on the planet and log in to the trading network, you don't notice any difference in the pricess.\n\n" +
                        "You are unsure if you've been swindled or if " + name + " just made a mistake.\n\nEither way, you " +
                        "lost " + NumberFormat.getNumberInstance(Locale.US).format(marketeerPrice) + " credits on this transaction.";
                okButtonText = "Ouch!";
                break;

            case 3:
                player.subtractMoney(marketeerPrice);
                increaseAllPrices();
                acceptResult = "Apparently, " + name + " has made a slight mistake!\n\nInstead of lowering all prices, it seems his marketeering " +
                        "efforts have resulted in a large price increase all over the market!\n\nYou're still" +
                        " out " + NumberFormat.getNumberInstance(Locale.US).format(marketeerPrice) + " credits.";
                okButtonText = "Hmmmm";
                break;
        }

    }


    public void princessPassenger() {


        long money;

        int rnd = MathUtils.random(1, 3);

        switch (rnd) {
            case 1:
                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                player.subtractMoney(money);
                acceptResult = "The princess tells you stories of forgotten frogs building castles in dead trees." +
                        "As you try to concentrate on flying the ship, the princess unexpectedly starts vomiting all over your ship controls.\n\n" +
                        "\"My goodness, what a mess,\" the princess slurs.\n\n" +
                        "The cleaning costs are estimated at " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits.";
                okButtonText = "Ouch!";
                break;
            case 2:
                money = MathUtils.random(player.getMoney() / 5, player.getMoney() / 2);
                player.addMoney(money);
                acceptResult = "The princess tells you stories of red moons with swampy skies and giant mushrooms inhabited by ancient wizards." +
                        "As you land on the planet, she gives you a pat on the back.\n\n" +
                        "\"My my, what excellent flying skills you have. A good deed cannot go unrewarded,\" the princess exclaims.\n\n" +
                        "As she leaves your ship, you notice that " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits" +
                        " have been added to your account.";
                okButtonText = "Nice!";
                break;
            case 3:
                acceptResult = "The princess tells you about the wonders of bee-keeping dogs and mechanical spiders.\n\nFascinated by her words, " +
                        "you overshoot your destination making you lose precious time." +
                        "As you dock on the planet, she quickly walks out muttering something about feeding the hornets and caking the ice.\n\n" +
                        "You are left behind wondering if it was all worth it.";
                okButtonText = "Continue";
                break;

        }


    }

    public void hitchHikerPassenger() {

        long money;

        int rnd = MathUtils.random(1, 3);

        switch (rnd) {
            case 1:
                acceptResult = "The young hitchhiker is very thankful. He tells you stories of all the wonders he has seen so far. Tales of ancient kingdoms on faraway moons, buried treasure" +
                        " and terrifying swamp monsters." +
                        "\n\nHis stories inspire you and renew your desire for discovery and exploration.\n" +
                        "\n\nAs you dock the ship at the port of " + player.getCurrentPlanet().getPlanetName() + ", the young hitchhiker wishes you good luck on your journey.";
                okButtonText = "Continue";
                break;
            case 2:
                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                player.addMoney(money);
                acceptResult = "The young hitchhiker is very thankful. He tells you stories of all the wonders he has seen so far. Tales of magic beans on toast, cloud cities" +
                        " and underground kingdoms.\n" +
                        "\"I cannot thank you enough for taking me here,\" the hitchhiker says as you dock the ship. \"I hope you don't mind a small tip?\"\n\n" +
                        "As he leaves your ship, you notice that " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits" +
                        " have been added to your account.";
                okButtonText = "Nice!";
                break;
            case 3:
                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                player.subtractMoney(money);
                acceptResult = "The young hitchhiker is very thankful. He tells you stories of distant moons inhabited by dusty ants " +
                        "and butterfly gardens." +
                        "\"Finally, we have arrived!\" the hitchhiker exclaims as you dock the ship. \"May we meet again someday!\"\n\n" +
                        "While the young hitchhiker leans in for a hug, he accidentally hits the steering controls causing the ship to lightly bump into the docking bay. " +
                        "You have been fined " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits by the port authority.";
                okButtonText = "Bummer!";
                break;

        }

    }

    public void petPassenger() {

        long money;

        int rnd = MathUtils.random(1, 3);

        switch (rnd) {
            case 1:
                acceptResult = "You take Flumpfer the dog with you on your trip. Flumpfer spends most of his time sleeping, occasionally waking up" +
                        " to stare out the window and gaze at the stars." +
                        "\n\nAs you dock the ship at the port of " + player.getCurrentPlanet().getPlanetName() + ", Flumpfer's adoption family happily picks him " +
                        "up and thanks you for your good deed.";
                okButtonText = "Continue";
                break;
            case 2:
                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                player.addMoney(money);
                acceptResult = "You take Flumpfer the dog with you on your trip. Flumpfer spends most of his time sleeping, occasionally waking up" +
                        " to stare out the window and gaze at the stars." +
                        "\n\nAs you dock the ship at the port of " + player.getCurrentPlanet().getPlanetName() + ", Flumpfer's adoption family happily picks him " +
                        "up.\n\nThey pay you " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits for your excellent delivery service.";
                okButtonText = "Nice!";
                break;
            case 3:
                money = MathUtils.random(player.getMoney() / 10, player.getMoney() / 5);
                player.subtractMoney(money);
                acceptResult = "You take Flumpfer the dog with you on your trip. Flumpfer spends most of his time sleeping, occasionally waking up" +
                        " to stare out the window and gaze at the stars." +
                        "\n\nAs you dock the ship at the port of " + player.getCurrentPlanet().getPlanetName() + ", Flumpfer's adoption family happily picks him " +
                        "up.\n\nWhen you return to the ship, you notice that Flumpfer peed all over your bed. " +
                        "\n\nThe cleaning costs are estimated at " + NumberFormat.getNumberInstance(Locale.US).format(money) + " credits.";
                okButtonText = "Ouch!";
                break;

        }


    }

    public void randomHitchhikers(int rndPassenger, String planet) {


        switch (rndPassenger) {
            case 1:
                acceptResult = "The merchant thanks you a thousand times and hops aboard your ship. She tells tales of fluffy beasts roaming the wilderness of moons " +
                        "she visited in a previous life. \n\nYou feel enriched by these stories.";
                break;
            case 2:
                acceptResult = "The minstrel is very grateful and hops aboard your ship. On the way to  " + planet + ", he sings you songs about " +
                        "powerful necro-warriors sailing on rivers of molten stone, floating cities on a planet without land. " +
                        "\n\nHis songs make you feel alive.";
                break;
            case 3:
                acceptResult = "While travelling to " + planet + " the young hacker works on installing the latest firmware update on your ship.\n\n" +
                        "\"There! All done!\" he says as you land on " + planet + ". He promises your trips will go by much faster now!" +
                        "\n\nYou feel much better now that your ship has been updated with the latest firmware.";
                break;
            case 4:
                acceptResult = "The Dark Lord tells you stories of ancient wars and forgotten rivalries. He speaks of underground castles, built atop " +
                        "mountains of fluff and boneless fish.\n\n" +
                        "\"Thank you for the ride, we will meet again very soon,\" he says as he leaves your ship." +
                        "\n\nYou feel as if his visit has refreshed the aura of your ship.";
                break;

            case 5:
                acceptResult = "On your way to " + planet + " you play a game of cribbage together. You arrive before the game is over, there are " +
                        "no winners or losers but everyone had lots of fun!\n\nYou feel more alive now.";
                break;

            case 6:
                player.addMoney(player.getMoney() / 10);
                acceptResult = "While travelling, the team vigorously cleans your ship. They are truly experts in their craft." +
                        "\n\nThey even manage to find some change between your couch cushions," +
                        " worth " + NumberFormat.getNumberInstance(Locale.US).format(player.getMoney() / 10) + " credits!";
                break;
        }

        okButtonText = "Continue";


    }
}