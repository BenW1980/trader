package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;


public class AboutScreen extends BaseScreen {

    private TextButton infoBtnLabel;

    public AboutScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        rootTable.setFillParent(true);

        TextButton moreGamesBtn = new TextButton("More games", skin);
        moreGamesBtn.getLabel().setFontScale(0.6f);

        TextButton privacyBtn = new TextButton("Privacy Policy", skin);
        privacyBtn.getLabel().setFontScale(0.6f);

        TextButton backBtn = new TextButton("Main Menu", skin);
        backBtn.getLabel().setFontScale(0.6f);

        infoBtnLabel = new TextButton(aboutTxt(), skin);
        infoBtnLabel.getLabel().setFontScale(0.5f);
        infoBtnLabel.getLabel().setAlignment(Align.center);
        infoBtnLabel.setTouchable(Touchable.disabled);

        privacyBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new PrivacyPolicyScreen(game));
            }
        });

        backBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
            }
        });

        moreGamesBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                Gdx.net.openURI("https://peanut-panda.itch.io/"); // open site
            }
        });

        rootTable.add(infoBtnLabel).width(470).height(200).padTop(295).padBottom(30);
        rootTable.row();
        rootTable.add(moreGamesBtn).padBottom(20).size(280, 70);
        rootTable.row();
        rootTable.add(privacyBtn).padBottom(20).size(280, 70);
        rootTable.row();
        rootTable.add(backBtn).padBottom(20).size(280, 70);

        stage.addActor(rootTable);
    }

    private String aboutTxt() {
        String txt;

        txt = "This game was created by\n\n Peanut Panda\n\nThanks for playing!";

        return txt;
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().draw(assetLoader.title(), 35, stage.getHeight() - 290, 410, 203);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
