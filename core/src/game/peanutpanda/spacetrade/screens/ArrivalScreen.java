package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.ScreenSize;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.Commodity;


public class ArrivalScreen extends BaseScreen {


    public ArrivalScreen(final SpaceTrade game) {
        super(game);
    }

    private String text() {

        return "\n" + arrivalEvent.randomEvent();
    }

    @Override
    public void show() {

        Table rootTable = new Table();
        Table planetDetailsLabelTable = new Table();
        rootTable.setFillParent(true);

        Label planetWord = new Label("You land safely on " + player.getCurrentPlanet().getPlanetName() + "", skin);
        Label planetName = new Label("", skin);
        Label factionWord = new Label("Ruling Faction :\n-----------------", skin);
        Label factionName = new Label(player.getCurrentPlanet().getFactionName(), skin);
        planetWord.setFontScale(0.5f);
        planetWord.setAlignment(Align.center);
        planetName.setFontScale(0.45f);
        planetName.setAlignment(Align.center);
        factionWord.setFontScale(0.5f);
        factionWord.setAlignment(Align.center);
        factionName.setFontScale(0.45F);
        factionName.setAlignment(Align.center);

        TextButton okButton = new TextButton("OK", skin);
        okButton.getLabel().setFontScale(0.7f);

        Label label = new Label(text(), skin, "blankLabel");
        label.setFontScale(0.47f);
        label.setAlignment(Align.topLeft);
        label.setTouchable(Touchable.disabled);
        label.setWrap(true);
        label.getStyle().background.setLeftWidth(10);
        label.getStyle().background.setTopHeight(10);
        label.getStyle().font.getData().setLineHeight(55);

        okButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });

        planetDetailsLabelTable.add(planetWord).padBottom(5).fillX().align(Align.left);
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(planetName).padBottom(25).fillX().align(Align.left);
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionWord).padBottom(5).fillX().align(Align.left);
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionName).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();

        rootTable.add(planetDetailsLabelTable).padBottom(90).padTop(30).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(label).size(ScreenSize.WIDTH.getSize() - 50, 350).padBottom(20);
        rootTable.row();
        rootTable.add(okButton).width(100).padTop(45).padBottom(75);

        stage.addActor(rootTable);

        String shortcodes = "";

        for (Commodity commodity : vars.commodities) {
            shortcodes += "  " + commodity.shortCode + " " + commodity.getPrice();
        }

        Label tickerTape = new Label(shortcodes + shortcodes, skin, "planetAll");
        tickerTape.setFontScale(0.4f);
        tickerTape.setPosition(0, ScreenSize.HEIGHT.getSize() - 240);
        tickerTape.addAction(Actions.addAction(Actions.moveTo(-2000, ScreenSize.HEIGHT.getSize() - 240, 100.0f)));
        stage.addActor(tickerTape);

        game.gameData.setContinueButton(false);
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().draw(assetLoader.infoBack(), 0, stage.getHeight() - 200, stage.getWidth(), 200);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
