package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.peanutpanda.spacetrade.AssetLoader;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.PlanetType;
import game.peanutpanda.spacetrade.gameobjects.Player;
import game.peanutpanda.spacetrade.gameobjects.TaxUrgency;
import game.peanutpanda.spacetrade.gameobjects.Vars;
import game.peanutpanda.spacetrade.random.ArrivalEvent;
import game.peanutpanda.spacetrade.random.TravelEvent;

import java.text.NumberFormat;
import java.util.Locale;


public abstract class BaseScreen implements Screen {

    protected Stage stage;
    protected OrthographicCamera camera;
    protected Viewport viewport;
    protected AssetLoader assetLoader;
    protected SpaceTrade game;
    protected Skin skin;
    protected Player player;
    protected BitmapFont gameBoyfnt;
    protected Label hudLabel;
    Vars vars;
    private String money;
    private String storage;
    private String week;
    private String taxesOwed;
    public ArrivalEvent arrivalEvent;
    public TravelEvent travelEvent;
    protected TaxUrgency taxUrgency;

    BaseScreen(SpaceTrade game) {
        this.game = game;
        this.vars = game.vars;
        this.player = vars.player;
        this.arrivalEvent = new ArrivalEvent(vars, player);
        this.travelEvent = new TravelEvent(vars, player, game);

        this.camera = game.camera;
        this.viewport = game.viewport;
        this.stage = new Stage(viewport);
        this.assetLoader = game.assLoader;
        this.camera.update();
        this.skin = assetLoader.skin();

        this.gameBoyfnt = assetLoader.assetManager.get("gameboy.ttf", BitmapFont.class);

        Gdx.input.setInputProcessor(stage);

        if (player != null) {
            updateHudInfo();
        }

        this.hudLabel = new Label(money + "\n" + storage + "\n" + week, skin);
        this.hudLabel.setFontScale(0.50f);

        taxUrgency = player.calculateTaxUrgency();
        vars.initParticleArray(assetLoader);


    }

    protected void visitPlanet() {

        if (player.getCurrentPlanet().getType().equals(PlanetType.CASINO.getSpecialType())) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new CasinoScreen(game));
        } else if (player.getCurrentPlanet().getType().equals(PlanetType.UPGRADE.getSpecialType())) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new UpgradeScreen(game));
        } else if (player.getCurrentPlanet().getType().equals(PlanetType.LOTTERY.getSpecialType())) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new LotteryScreen(game));
        } else if (player.getCurrentPlanet().getType().equals(PlanetType.ORACLE.getSpecialType())) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new OracleScreen(game));
        } else if (player.getCurrentPlanet().getType().equals(PlanetType.CAPITAL.getSpecialType())) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new CapitalScreen(game));
        }
    }

    protected void updateHudInfo() {

        money = "Money          : " + NumberFormat.getNumberInstance(Locale.US).format(player.getMoney()) + " cr";
        storage = "Cargo          : " + player.getCargoOnBoard() + "-" + player.getCargoHoldSize();
        week = "Week            : " + player.getWeek() + "-" + player.MAX_WEEK;
        taxesOwed = "Taxes Owed : " + NumberFormat.getNumberInstance(Locale.US).format(player.getTaxesOwed()) + " cr";
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();


        if (player != null) {
            updateHudInfo();
        }

        hudLabel.setText(money + "\n" + storage + "\n" + week + "\n" + taxesOwed);

        stage.act();

        if (!(this instanceof IntroScreen)) {
            stage.getBatch().begin();
            stage.getBatch().draw(assetLoader.background(), 0, 0, stage.getWidth(), stage.getHeight());
            stage.getBatch().draw(assetLoader.infoBack(), 0, (stage.getHeight() - 15) - 120, stage.getWidth(), 135);
            stage.getBatch().end();
            stage.draw();
        }

        System.out.println(player.isWarnedAboutTaxes + " - " + player.getTaxUrgencyCounter());

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        game.assLoader.assetManager.dispose();
    }
}