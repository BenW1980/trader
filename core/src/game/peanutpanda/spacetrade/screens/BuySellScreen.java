package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.Commodity;

import java.text.NumberFormat;
import java.util.Locale;

public class BuySellScreen extends BaseScreen {

    private TextButton boughtAtButtonLabel;
    private TextButton amountShipPlanetButtonLabel;
    private TextButton buy1;
    private TextButton buy10;
    private TextButton buyMax;
    private TextButton sell1;
    private TextButton sell10;
    private TextButton sellMax;
    private Commodity commodity;

    public BuySellScreen(final SpaceTrade game, final Commodity commodity) {
        super(game);
        this.commodity = commodity;

        Table rootTable = new Table();
        rootTable.setFillParent(true);
        Table upperLableTable = new Table();
        Table buyButtonTable = new Table();
        Table sellButtonTable = new Table();
        Table lowerLableTable = new Table();
        Table lowerButtonTable = new Table();
        TextButton mainBtn = new TextButton("MAIN", skin);
        TextButton marketBtn = new TextButton("MARKET", skin);
        TextButton travelBtn = new TextButton("TRAVEL", skin);
        mainBtn.getLabel().setFontScale(0.55f);
        marketBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(mainBtn).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(marketBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        mainBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        TextButton commodityNameButtonLabel = new TextButton(commodity.getCommodityName() + "\n" + NumberFormat.getNumberInstance(Locale.US).format(commodity.getPrice()) + " cr", skin);
        commodityNameButtonLabel.getLabel().setFontScale(0.75f);
        commodityNameButtonLabel.setTouchable(Touchable.disabled);

        amountShipPlanetButtonLabel = new TextButton("On ship : " + commodity.getAmountOnShip() + "\nOn planet : " + commodity.getAmountOnPlanet(), skin);
        amountShipPlanetButtonLabel.getLabel().setFontScale(0.45f);
        amountShipPlanetButtonLabel.setTouchable(Touchable.disabled);

        boughtAtButtonLabel = new TextButton("Bought at : " + NumberFormat.getNumberInstance(Locale.US).format(commodity.calculateBoughtAtPrice()) + "\nProfit : " + (int) percentage() + "%", skin);
        boughtAtButtonLabel.setTouchable(Touchable.disabled);
        boughtAtButtonLabel.getLabel().setFontScale(0.6f);
        boughtAtButtonLabel.getLabel().setAlignment(Align.left);
        boughtAtButtonLabel.padLeft(20).padRight(20);

        buy1 = new TextButton("BUY\n  1", skin);
        buy1.getLabel().setFontScale(0.75f);

        buy10 = new TextButton("BUY\n 10", skin);
        buy10.getLabel().setFontScale(0.75f);
        buyMax = new TextButton("BUY\nMAX", skin);
        buyMax.getLabel().setFontScale(0.75f);

        sell1 = new TextButton("SELL\n  1", skin);
        sell1.getLabel().setFontScale(0.75f);
        sell10 = new TextButton("SELL\n 10", skin);
        sell10.getLabel().setFontScale(0.75f);
        sellMax = new TextButton("SELL\nMAX", skin);
        sellMax.getLabel().setFontScale(0.75f);

        addListeners();

        upperLableTable.add(commodityNameButtonLabel).top().center().padTop(35).width(stage.getWidth() - 20).height(100);
        upperLableTable.row();
        upperLableTable.add(amountShipPlanetButtonLabel).top().center().padBottom(20).width(stage.getWidth() - 20).height(100);

        buyButtonTable.add(buy1).pad(10).height(80).width(110);
        buyButtonTable.add(buy10).pad(10).height(80).width(110);
        buyButtonTable.add(buyMax).pad(10).height(80).width(110);

        sellButtonTable.add(sell1).pad(10).height(80).width(110);
        sellButtonTable.add(sell10).pad(10).height(80).width(110);
        sellButtonTable.add(sellMax).pad(10).height(80).width(110);

        lowerLableTable.add(boughtAtButtonLabel).width(stage.getWidth() - 40);

        rootTable.add(hudLabel).pad(195, 20, 20, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(upperLableTable);
        rootTable.row();
        rootTable.add(buyButtonTable);
        rootTable.row();
        rootTable.add(sellButtonTable).padBottom(30);
        rootTable.row();
        rootTable.add(lowerLableTable).bottom().left().pad(0, 20, 30, 0);
        rootTable.row();
        rootTable.add(lowerButtonTable).padBottom(175);

        stage.addActor(rootTable);

    }

    private void addListeners() {
        buy1.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.buyOne(commodity);
                commodity.addToBoughtAtPrice(commodity.getPrice(), 1);
                game.gameData.saveGame(vars, player);
            }
        });
        buy10.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.buyTen(commodity);
                commodity.addToBoughtAtPrice(commodity.getPrice(), 10);
                game.gameData.saveGame(vars, player);
            }
        });
        buyMax.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.buyMax(commodity);
                commodity.addToBoughtAtPrice(commodity.getPrice(), player.getAmountToAdd());
                player.setAmountToAdd(0);
                game.gameData.saveGame(vars, player);
            }
        });
        sell1.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.sellOne(commodity);
                game.gameData.saveGame(vars, player);
            }
        });
        sell10.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.sellTen(commodity);
                game.gameData.saveGame(vars, player);
            }
        });
        sellMax.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.sellMax(commodity);
                game.gameData.saveGame(vars, player);
            }
        });

    }

    private void disableButtons() {

        if (commodity.getAmountOnPlanet() < 10 || player.getMoney() < commodity.getPrice() * 10 || !player.hasRoomLeftForTen()) {
            buy10.setTouchable(Touchable.disabled);
            buy10.setDisabled(true);
        } else {
            buy10.setTouchable(Touchable.enabled);
            buy10.setDisabled(false);
        }

        if (commodity.getAmountOnPlanet() < 1 || player.getMoney() < commodity.getPrice() || !player.hasRoomLeft()) {
            buy1.setTouchable(Touchable.disabled);
            buyMax.setTouchable(Touchable.disabled);
            buy1.setDisabled(true);
            buyMax.setDisabled(true);
        } else {
            buy1.setTouchable(Touchable.enabled);
            buyMax.setTouchable(Touchable.enabled);
            buy1.setDisabled(false);
            buyMax.setDisabled(false);
        }

        if (commodity.getAmountOnShip() < 10) {
            sell10.setTouchable(Touchable.disabled);
            sell10.setDisabled(true);
        } else {
            sell10.setTouchable(Touchable.enabled);
            sell10.setDisabled(false);
        }

        if (commodity.getAmountOnShip() < 1) {
            sell1.setTouchable(Touchable.disabled);
            sellMax.setTouchable(Touchable.disabled);
            sell1.setDisabled(true);
            sellMax.setDisabled(true);
        } else {
            sell1.setTouchable(Touchable.enabled);
            sellMax.setTouchable(Touchable.enabled);
            sell1.setDisabled(false);
            sellMax.setDisabled(false);
        }
    }

    private double percentage() {

        double number1 = commodity.calculateBoughtAtPrice();
        double number2 = commodity.getPrice();

        double difference = number2 - number1;

        if (number1 != 0) {
            return difference;
        }

        return 0;
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        boughtAtButtonLabel.setText("Bought at : " + commodity.calculateBoughtAtPrice() + "\nProfit : " + (int) percentage() + "%");

        if (commodity.getAmountOnShip() == 0) {
            commodity.emptyBoughtAtPrice();
        }

        disableButtons();
        amountShipPlanetButtonLabel.setText("On ship : " + commodity.getAmountOnShip() + "\nOn planet : " + commodity.getAmountOnPlanet());

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
