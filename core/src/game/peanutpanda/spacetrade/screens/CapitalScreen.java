package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;


public class CapitalScreen extends BaseScreen {

    private TextButton btnLable;
    private TextButton payTaxesBtn;
    private int amountToPay;

    public CapitalScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        Table buttonTable = new Table();
        Table planetDetailsLabelTable = new Table();
        rootTable.setFillParent(true);

        Label planetWord = new Label("Current Planet : \n----------------", skin);
        Label planetName = new Label(player.getCurrentPlanet().getPlanetName(), skin);
        Label factionWord = new Label("Ruling Faction : \n----------------", skin);
        Label factionName = new Label(player.getCurrentPlanet().getFactionName(), skin);
        planetWord.setFontScale(0.5f);
        planetName.setFontScale(0.45f);
        factionWord.setFontScale(0.5f);
        factionName.setFontScale(0.45F);

        Table lowerButtonTable = new Table();
        TextButton mainBtn = new TextButton("MAIN", skin);
        TextButton marketBtn = new TextButton("MARKET", skin);
        TextButton travelBtn = new TextButton("TRAVEL", skin);
        mainBtn.getLabel().setFontScale(0.55f);
        marketBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(mainBtn).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(marketBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        mainBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        planetDetailsLabelTable.add(planetWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(planetName).padBottom(25).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionName).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();

        btnLable = new TextButton("  " + taxText(), skin);
        btnLable.getLabel().setFontScale(0.50f);
        btnLable.getLabel().setAlignment(Align.left);
        btnLable.setTouchable(Touchable.disabled);
        btnLable.getLabel().setWrap(true);

        payTaxesBtn = new TextButton(payBtnTxt(), skin);
        payTaxesBtn.getLabel().setFontScale(0.6f);

        payTaxesBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.setMoney(player.getMoney() - amountToPay);
                player.setTaxesOwed(player.getTaxesOwed() - amountToPay);
                game.gameData.saveGame(vars, player);
            }
        });

        buttonTable.pad(5, 0, 40, 0);

        buttonTable.add(payTaxesBtn).padBottom(0).padTop(40).height(70).width(410);

        rootTable.add(hudLabel).pad(50, 20, 20, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(planetDetailsLabelTable).padLeft(18).padTop(25).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(btnLable).width(470).height(150).padTop(75);
        rootTable.row();
        rootTable.add(buttonTable);
        rootTable.row();
        rootTable.add(lowerButtonTable).padBottom(35).padTop(20);

        stage.addActor(rootTable);

    }

    private String payBtnTxt() {
        return "Pay\n- " + NumberFormat.getNumberInstance(Locale.US).format(amountToPay) + " cr -";
    }

    private String taxText() {

        String txt = "  Taxes owed : " + NumberFormat.getNumberInstance(Locale.US).format(player.getTaxesOwed()) + " cr\n" +
                "  Status        : " + taxUrgency.name() + "\n\n" +
                "  Current tax rate : " + player.getTaxRate() + " pct";

        return txt;
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        taxUrgency = player.calculateTaxUrgency();

        if (player.getTaxesOwed() > 1000000) {
            amountToPay = player.getTaxesOwed() / 5;
        } else if (player.getTaxesOwed() > 500000 && player.getTaxesOwed() <= 1000000) {
            amountToPay = player.getTaxesOwed() / 4;
        } else if (player.getTaxesOwed() > 100000 && player.getTaxesOwed() <= 500000) {
            amountToPay = player.getTaxesOwed() / 3;
        } else if (player.getTaxesOwed() > 10000 && player.getTaxesOwed() <= 100000) {
            amountToPay = player.getTaxesOwed() / 2;
        } else if (player.getTaxesOwed() <= 10000) {
            amountToPay = player.getTaxesOwed();
        }

        stage.act();

        btnLable.setText(taxText());

        if (player.getTaxesOwed() == 0) {
            payTaxesBtn.setDisabled(true);
            payTaxesBtn.setTouchable(Touchable.disabled);
            payTaxesBtn.setText("All done!");
        } else {
            payTaxesBtn.setDisabled(false);
            payTaxesBtn.setTouchable(Touchable.enabled);
            payTaxesBtn.setText(payBtnTxt());

            if (player.getMoney() < amountToPay) {
                payTaxesBtn.setDisabled(true);
                payTaxesBtn.setTouchable(Touchable.disabled);
            } else {
                payTaxesBtn.setDisabled(false);
                payTaxesBtn.setTouchable(Touchable.enabled);
            }

        }


        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.infoBack(), 0, (stage.getHeight() - 15) - 330, stage.getWidth(), 200);
        stage.getBatch().end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
