package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.AssetLoader;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.SlotItem;
import game.peanutpanda.spacetrade.gameobjects.SlotItemType;

import java.text.NumberFormat;
import java.util.Locale;


public class CasinoScreen extends BaseScreen {

    private final int MAX_SPIN = 3;
    private TextButton diceLabelBtn;
    private TextButton rollBtn;
    private TextButton addBet;
    private TextButton removeBet;
    private Array<SlotItem> slotItems1;
    private Array<SlotItem> slotItems2;
    private Array<SlotItem> slotItems3;
    private int counter1;
    private int counter2;
    private int counter3;
    private float timePassed;
    private int result1;
    private int result2;
    private int result3;
    private float amountBet = 0.0f;
    private int amountWon;
    private boolean spun, spunOnce, firstTierWin, secondTierWin;
    private float modifier;
    private int betAmountPerClick, maxBetAmount;

    TextButton mainBtn;
    TextButton marketBtn;
    TextButton travelBtn;

    public CasinoScreen(final SpaceTrade game) {
        super(game);

        if (player.getWeek() >= 0 && player.getWeek() < 10) {
            betAmountPerClick = 5000;
        } else if (player.getWeek() >= 10 && player.getWeek() < 20) {
            betAmountPerClick = 7500;
        } else if (player.getWeek() >= 20 && player.getWeek() < 30) {
            betAmountPerClick = 10000;
        } else if (player.getWeek() >= 30) {
            betAmountPerClick = 15000;
        }

        maxBetAmount = betAmountPerClick * 10;

        slotItems1 = createArray(assetLoader);
        slotItems2 = createArray(assetLoader);
        slotItems3 = createArray(assetLoader);
        slotItems1.shuffle();
        slotItems2.shuffle();
        slotItems3.shuffle();

        Table rootTable = new Table();
        Table buttonTable = new Table();
        Table planetDetailsLabelTable = new Table();
        rootTable.setFillParent(true);

        Label planetWord = new Label("Current Planet : \n----------------", skin);
        Label planetName = new Label(player.getCurrentPlanet().getPlanetName(), skin);
        Label factionWord = new Label("Ruling Faction : \n----------------", skin);
        Label factionName = new Label(player.getCurrentPlanet().getFactionName(), skin);
        planetWord.setFontScale(0.5f);
        planetName.setFontScale(0.45f);
        factionWord.setFontScale(0.5f);
        factionName.setFontScale(0.45F);

        Table lowerButtonTable = new Table();
        mainBtn = new TextButton("MAIN", skin);
        marketBtn = new TextButton("MARKET", skin);
        travelBtn = new TextButton("TRAVEL", skin);
        mainBtn.getLabel().setFontScale(0.55f);
        marketBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(mainBtn).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(marketBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        mainBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        planetDetailsLabelTable.add(planetWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(planetName).padBottom(25).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionName).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();

        diceLabelBtn = new TextButton("  Current bet : 0 cr", skin);
        diceLabelBtn.getLabel().setFontScale(0.45f);
        diceLabelBtn.getLabel().setAlignment(Align.center);
        diceLabelBtn.setTouchable(Touchable.disabled);

        result1 = MathUtils.random(0, slotItems1.size - 1);
        result2 = MathUtils.random(0, slotItems2.size - 1);
        result3 = MathUtils.random(0, slotItems3.size - 1);

        rollBtn = new TextButton("Spin", skin);
        rollBtn.getLabel().setFontScale(0.6f);
        addBet = new TextButton("Bet\n" + NumberFormat.getNumberInstance(Locale.US).format(betAmountPerClick) + " cr", skin);
        addBet.getLabel().setFontScale(0.53f);
        removeBet = new TextButton("Remove\n" + NumberFormat.getNumberInstance(Locale.US).format(betAmountPerClick) + " cr", skin);
        removeBet.getLabel().setFontScale(0.53f);

        rollBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                player.subtractMoney((int) amountBet);
                player.addSpin();
                spun = true;
                result1 = MathUtils.random(0, slotItems1.size - 1);
                result2 = MathUtils.random(0, slotItems2.size - 1);
                result3 = MathUtils.random(0, slotItems3.size - 1);
                game.gameData.saveGame(vars, player);
            }
        });

        removeBet.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                amountBet -= betAmountPerClick;
            }
        });

        addBet.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if (player.getTimesSpun() < MAX_SPIN) {
                    if (amountBet + betAmountPerClick <= player.getMoney() && amountBet < maxBetAmount) {
                        amountBet += betAmountPerClick;
                    }
                } else {
                    player.setPlayedMaxTimes(true);
                    addBet.setDisabled(false);
                    addBet.setTouchable(Touchable.enabled);
                }
            }
        });

        buttonTable.pad(10, 0, 20, 0);

        buttonTable.add(removeBet).padBottom(0).padTop(15).height(70).width(150);
        buttonTable.add(rollBtn).padBottom(0).padTop(15).height(70).width(150);
        buttonTable.add(addBet).padBottom(0).padTop(15).height(70).width(150);

        rootTable.add(hudLabel).pad(95, 20, 20, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(planetDetailsLabelTable).padLeft(18).padTop(28).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(diceLabelBtn).width(380).height(100).padTop(180).padRight(5);
        rootTable.row();
        rootTable.add(buttonTable).width(100);
        rootTable.row();
        rootTable.add(lowerButtonTable).padBottom(80).padTop(0);

        stage.addActor(rootTable);
    }

    private Array<SlotItem> createArray(AssetLoader assetLoader) {
        Array<SlotItem> array = new Array<>();

        array.add(new SlotItem(SlotItemType.CHERRY, assetLoader));
        array.add(new SlotItem(SlotItemType.CHERRY, assetLoader));
        array.add(new SlotItem(SlotItemType.CHERRY, assetLoader));
        array.add(new SlotItem(SlotItemType.CHERRY, assetLoader));
        array.add(new SlotItem(SlotItemType.CHERRY, assetLoader));
        array.add(new SlotItem(SlotItemType.CHERRY, assetLoader));
        array.add(new SlotItem(SlotItemType.APPLE, assetLoader));
        array.add(new SlotItem(SlotItemType.APPLE, assetLoader));
        array.add(new SlotItem(SlotItemType.APPLE, assetLoader));
        array.add(new SlotItem(SlotItemType.APPLE, assetLoader));
        array.add(new SlotItem(SlotItemType.SHOE, assetLoader));
        array.add(new SlotItem(SlotItemType.SHOE, assetLoader));
        array.add(new SlotItem(SlotItemType.CLOVER, assetLoader));
        array.add(new SlotItem(SlotItemType.CLOVER, assetLoader));
        array.add(new SlotItem(SlotItemType.BELL, assetLoader));
        array.add(new SlotItem(SlotItemType.BELL, assetLoader));
        array.add(new SlotItem(SlotItemType.DIAMOND, assetLoader));
        array.add(new SlotItem(SlotItemType.DOLLAR, assetLoader));
        array.add(new SlotItem(SlotItemType.SEVEN, assetLoader));

        return array;
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        String winText;

        if (spunOnce) {
            winText = "\n   You've won " + NumberFormat.getNumberInstance(Locale.US).format(amountWon) + " cr!";
        } else {
            winText = "";
        }

        if (spun) {
            mainBtn.setDisabled(true);
            marketBtn.setDisabled(true);
            travelBtn.setDisabled(true);
            mainBtn.setTouchable(Touchable.disabled);
            marketBtn.setTouchable(Touchable.disabled);
            travelBtn.setTouchable(Touchable.disabled);

            removeBet.setDisabled(true);
            removeBet.setTouchable(Touchable.disabled);
            addBet.setDisabled(true);
            addBet.setTouchable(Touchable.disabled);
            firstTierWin = false;
            secondTierWin = false;
            winText = "";
            rollBtn.setDisabled(true);
            rollBtn.setTouchable(Touchable.disabled);

            timePassed += delta * 10;

            if (timePassed < 10) {
                counter1++;
            } else {
                counter1 = result1;
            }

            if (timePassed < 15) {
                counter2++;
            } else {
                counter2 = result2;
            }

            if (timePassed < 20) {
                counter3++;
            } else {
                counter3 = result3;
                spun = false;
                timePassed = 0;
                spunOnce = true;
                checkWinTiers();
                amountWon = (int) payOut();
                player.addMoney(amountWon);
                amountBet = 0;
                if (amountWon > 0) {
                    assetLoader.moneyWin().play();
                } else {
                    assetLoader.moneyLose().play();
                }
            }

            if (counter1 > slotItems1.size - 1) {
                counter1 = 0;
            }
            if (counter2 > slotItems2.size - 1) {
                counter2 = 0;
            }
            if (counter3 > slotItems3.size - 1) {
                counter3 = 0;
            }

        } else {
            if (amountBet != 0) {
                rollBtn.setDisabled(false);
                rollBtn.setTouchable(Touchable.enabled);
            } else {
                rollBtn.setDisabled(true);
                rollBtn.setTouchable(Touchable.disabled);
            }
        }

        if (!spun) {

            mainBtn.setDisabled(false);
            marketBtn.setDisabled(false);
            travelBtn.setDisabled(false);
            mainBtn.setTouchable(Touchable.enabled);
            marketBtn.setTouchable(Touchable.enabled);
            travelBtn.setTouchable(Touchable.enabled);

            if (amountBet == 0) {
                removeBet.setDisabled(true);
                removeBet.setTouchable(Touchable.disabled);
            } else {
                removeBet.setDisabled(false);
                removeBet.setTouchable(Touchable.enabled);
            }

            if (amountBet + betAmountPerClick > player.getMoney()) {
                addBet.setDisabled(true);
                addBet.setTouchable(Touchable.disabled);
            } else {
                addBet.setDisabled(false);
                addBet.setTouchable(Touchable.enabled);
            }

            if (amountBet == maxBetAmount) {
                addBet.setDisabled(true);
                addBet.setTouchable(Touchable.disabled);
            } else {
                addBet.setDisabled(false);
                addBet.setTouchable(Touchable.enabled);
            }

        }

        if (player.isPlayedMaxTimes()) {
            removeBet.setDisabled(true);
            removeBet.setTouchable(Touchable.disabled);
            addBet.setDisabled(true);
            addBet.setTouchable(Touchable.disabled);
            rollBtn.setDisabled(true);
            rollBtn.setTouchable(Touchable.disabled);
            diceLabelBtn.setText("   The casino is closed.\n   Come back next week!");
        } else {
            diceLabelBtn.setText("   Current bet : " + NumberFormat.getNumberInstance(Locale.US).format((int) amountBet) + " Cr\n" + winText);
        }


        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.infoBack(), 0, (stage.getHeight() - 15) - 330, stage.getWidth(), 200);
        stage.getBatch().draw(assetLoader.slotsBackground(), 48, stage.getHeight() / 2 - 80, 380, 120);
        stage.getBatch().draw(slotItems1.get(counter1).getTexture(), 68, stage.getHeight() / 2 - 70, 100, 100);
        stage.getBatch().draw(slotItems2.get(counter2).getTexture(), 188, stage.getHeight() / 2 - 70, 100, 100);
        stage.getBatch().draw(slotItems3.get(counter3).getTexture(), 308, stage.getHeight() / 2 - 70, 100, 100);
        stage.getBatch().end();

        stage.draw();

        System.out.println(modifier);

    }

    private float payOut() {

        float result = 0.0f;

        if (firstTierWin) {

            if (modifier == 2.0f) {
                result += amountBet * 1.5f;

            } else if (modifier == 4.0f) {
                result += amountBet * 1.8f;

            } else if (modifier == 6.0f) {
                result += amountBet * 2.0f;

            } else if (modifier == 8.0f) {
                result += amountBet * 2.2f;

            } else if (modifier == 10.0f) {
                result += amountBet * 2.5f;

            } else if (modifier == 12.0f) {
                result += amountBet * 2.8f;

            } else if (modifier == 14.0f) {
                result += amountBet * 3.2f;

            } else if (modifier == 16.0f) {
                result += amountBet * 3.6f;
            }


        } else if (secondTierWin) {

            if (modifier == 2.0f) {
                result += amountBet * 2.2f;

            } else if (modifier == 4.0f) {
                result += amountBet * 2.5f;

            } else if (modifier == 6.0f) {
                result += amountBet * 2.8f;

            } else if (modifier == 8.0f) {
                result += amountBet * 3.5f;

            } else if (modifier == 10.0f) {
                result += amountBet * 4.0f;

            } else if (modifier == 12.0f) {
                result += amountBet * 5.0f;

            } else if (modifier == 14.0f) {
                result += amountBet * 6.0f;

            } else if (modifier == 16.0f) {
                result += amountBet * 7.0f;
            }
        }

        return result;
    }


    private void checkWinTiers() {

        //1+2
        if (slotItems1.get(counter1).getId() == slotItems2.get(counter2).getId() && slotItems1.get(counter1).getId() != slotItems3.get(counter3).getId()) {
            firstTierWin = true;
            secondTierWin = false;
            modifier = (slotItems1.get(counter1).getId() + slotItems2.get(counter2).getId()) + 0.0f;
            return;
        }

        //2+3
        if (slotItems2.get(counter2).getId() == slotItems3.get(counter3).getId() && slotItems2.get(counter2).getId() != slotItems1.get(counter1).getId()) {
            firstTierWin = true;
            secondTierWin = false;
            modifier = (slotItems2.get(counter2).getId() + slotItems3.get(counter3).getId()) + 0.0f;
            return;
        }

        //1+3
        if (slotItems1.get(counter1).getId() == slotItems3.get(counter3).getId() && slotItems1.get(counter1).getId() != slotItems2.get(counter2).getId()) {
            firstTierWin = true;
            secondTierWin = false;
            modifier = (slotItems1.get(counter1).getId() + slotItems3.get(counter3).getId()) + 0.0f;
            return;
        }

        //1+2+3
        if (slotItems1.get(counter1).getId() == slotItems2.get(counter2).getId() && slotItems1.get(counter1).getId() == slotItems3.get(counter3).getId()) {
            firstTierWin = false;
            secondTierWin = true;
            modifier = (slotItems1.get(counter1).getId() + slotItems2.get(counter2).getId()) + 0.0f;
            return;
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
