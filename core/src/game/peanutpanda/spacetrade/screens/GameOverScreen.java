package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;


public class GameOverScreen extends BaseScreen {

    private TextButton outtroBtnLabler;
    private TextButton backBtn;

    public GameOverScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        rootTable.setFillParent(true);

        backBtn = new TextButton("Back to menu", skin);
        backBtn.getLabel().setFontScale(0.6f);

        outtroBtnLabler = new TextButton(endingTxt(), skin);
        outtroBtnLabler.getLabel().setFontScale(0.41f);
        outtroBtnLabler.getLabel().setWrap(true);
        outtroBtnLabler.getLabel().getStyle().font.getData().setLineHeight(55);
        outtroBtnLabler.setTouchable(Touchable.disabled);

        backBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
            }
        });

        rootTable.add(outtroBtnLabler).width(470).height(340).padTop(220).padBottom(60);
        rootTable.row();
        rootTable.add(backBtn).width(300);

        stage.addActor(rootTable);

        game.gameData.setContinueButton(false);

    }

    private String endingTxt() {
        long finalAmount = player.getMoney() - player.getTaxesOwed();
        game.gameData.saveHighscore(finalAmount);
        String txt = "Game Over!\n\n";

        if (player.getMoney() >= vars.moneyToWin) {
            txt += "Congratulations! You've made " + NumberFormat.getNumberInstance(Locale.US).format(player.getMoney()) + " credits in " + player.MAX_WEEK + " weeks!\n\n" +
                    "You have earned the title of \nelite space trucker.\n\nYour name will now forever be written in the annals of Space Trading.";

        } else {
            txt += "You did not make the goal of earning " + NumberFormat.getNumberInstance(Locale.US).format(vars.moneyToWin) +
                    " credits in " + player.MAX_WEEK + " weeks.\n\n" +
                    "One day your name will be written \nin the annals of Space Trading,\nbut that day is not today ...";
        }


        return txt + "\n\nEarnings : " + NumberFormat.getNumberInstance(Locale.US).format(finalAmount) + " cr\n" +
                "Highest  : " + NumberFormat.getNumberInstance(Locale.US).format(game.gameData.highscore()) + " cr";
    }

    @Override
    public void show() {

        assetLoader.theme().play();

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().draw(assetLoader.title(), 35, stage.getHeight() - 250, 410, 203);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
