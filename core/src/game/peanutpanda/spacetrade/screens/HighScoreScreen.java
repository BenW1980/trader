package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;


public class HighScoreScreen extends BaseScreen {

    private TextButton infoBtnLabel;

    public HighScoreScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        rootTable.setFillParent(true);

        TextButton backBtn = new TextButton("Main Menu", skin);
        backBtn.getLabel().setFontScale(0.6f);

        infoBtnLabel = new TextButton(aboutTxt(), skin);
        infoBtnLabel.getLabel().setFontScale(0.5f);
        infoBtnLabel.getLabel().setAlignment(Align.center);
        infoBtnLabel.setTouchable(Touchable.disabled);

        backBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
            }
        });


        rootTable.add(infoBtnLabel).width(470).height(200).padTop(295).padBottom(80);
        rootTable.row();
        rootTable.add(backBtn).padBottom(0).size(280, 70);

        stage.addActor(rootTable);
    }

    private String aboutTxt() {
        String txt;

        txt = "highest earnings\n----------------\n\n\n"
                + NumberFormat.getNumberInstance(Locale.US).format(game.gameData.highscore()) + " cr\n";

        return txt;
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().draw(assetLoader.title(), 35, stage.getHeight() - 290, 410, 203);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
