package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;


public class IntroScreen extends BaseScreen {

    private TextButton introBtnLabel;
    private TextButton startBtn;

    public IntroScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        rootTable.setFillParent(true);


        startBtn = new TextButton("Start Trucking", skin);
        startBtn.getLabel().setFontScale(0.6f);

        introBtnLabel = new TextButton(introText(), skin);
        introBtnLabel.getLabel().setFontScale(0.41f);
        introBtnLabel.setTouchable(Touchable.disabled);

        startBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TransitScreen(game));
            }
        });

        rootTable.add(introBtnLabel).width(470).height(215).padTop(295).padBottom(60);
        rootTable.row();
        rootTable.add(startBtn).width(300);

        stage.addActor(rootTable);
    }

    private String introText() {
        String txt;

        txt = "You have just inherited a space ship.\nGreat stuff!\n\nYour goal : " +
                "Earn " + NumberFormat.getNumberInstance(Locale.US).format(vars.moneyToWin) + " credits.\n\n" +
                "You have " + player.MAX_WEEK + " weeks. Good luck!";

        return txt;
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().draw(assetLoader.title(), 35, stage.getHeight() - 290, 410, 203);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
