package game.peanutpanda.spacetrade.screens;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.peanutpanda.spacetrade.SpaceTrade;


public class LoadingScreen implements Screen {

    SpaceTrade game;
    private Sprite pandaTxtSprite;
    private SpriteBatch batch;
    private float timePassed, timeAfterSoundPlayed;
    private boolean soundPlayed;
    private boolean logoInPlace;
    private Sound coin;

    public LoadingScreen(SpaceTrade game) {
        this.game = game;
        this.batch = new SpriteBatch();
        this.pandaTxtSprite = new Sprite(new Texture(Gdx.files.internal("pepaTxt.png")));
        coin = Gdx.audio.newSound(Gdx.files.internal("sounds/coin.ogg"));
        pandaTxtSprite.setBounds(50, 800, 382, 40);

    }

    @Override
    public void show() {
        game.assLoader.startLoading();

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(142f / 255f, 185f / 255f, 120f / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();

        if (game.assLoader.assetManager.update()) {
            pandaTxtSprite.setY(800 - timePassed);
            pandaTxtSprite.draw(batch);


            if (pandaTxtSprite.getY() > 380) {
                timePassed += delta * 150;
            } else {
                logoInPlace = true;
                pandaTxtSprite.setY(378);

            }

            if (logoInPlace) {
                timeAfterSoundPlayed += delta * 10;
            }

            if (logoInPlace && !soundPlayed) {
                coin.setPitch(coin.play(), 0.60f);
                soundPlayed = true;
            }

            if (logoInPlace && soundPlayed && timeAfterSoundPlayed > 10) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
            }
        }

        batch.end();

        game.camera.update();
        batch.setProjectionMatrix(game.camera.combined);

    }

    @Override
    public void resize(int width, int height) {
        game.viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        pandaTxtSprite.getTexture().dispose();
        coin.dispose();
    }


}
