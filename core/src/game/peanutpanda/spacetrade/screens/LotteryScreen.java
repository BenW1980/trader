package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;

public class LotteryScreen extends BaseScreen {

    private final int ticketPrice = 500;
    private final int twoNumbersWinning = 2500;
    private TextButton nr1;
    private TextButton nr2;
    private TextButton nr3;
    private TextButton nr4;
    private TextButton nr5;
    private TextButton nr6;
    private TextButton choice1;
    private TextButton choice2;
    private TextButton choice3;
    private TextButton result1;
    private TextButton result2;
    private TextButton result3;
    private TextButton reset;
    private TextButton play;
    private TextButton thisWeekBtnLable;
    private Array<String> chosenNumberArray;
    private Array<String> winningNumberArray;
    private Array<String> allNumbersArray;
    private Array<TextButton> choiceBtnsArray;
    private int jackpot;
    private boolean playPressed;

    private float timePassed;

    public LotteryScreen(final SpaceTrade game) {
        super(game);

        this.jackpot = vars.jackpot;

        this.chosenNumberArray = new Array<>();
        this.winningNumberArray = new Array<>();
        this.allNumbersArray = new Array<>();
        this.allNumbersArray.addAll("1", "2", "3", "4", "5", "6");
        this.allNumbersArray.shuffle();

        if (vars.oracleWinningNumbers.isEmpty()) {
            this.winningNumberArray.addAll(allNumbersArray.get(0), allNumbersArray.get(1), allNumbersArray.get(2));
            this.winningNumberArray.sort();

        } else {
            winningNumberArray.addAll(vars.oracleWinningNumbers.get(0), vars.oracleWinningNumbers.get(1), vars.oracleWinningNumbers.get(2));
            this.winningNumberArray.sort();
        }


        this.choiceBtnsArray = new Array<>();

        Table rootTable = new Table();
        rootTable.setFillParent(true);
        Table upperLableTable = new Table();
        Table upperNumbers = new Table();
        Table middleNumbers = new Table();
        Table chosenNumbers = new Table();
        Table winningnumbers = new Table();
        Table chosenLableTable = new Table();
        Table winningLableTable = new Table();
        Table lowerButtonTable = new Table();
        Table thisWeekTable = new Table();
        TextButton mainBtn = new TextButton("MAIN", skin);
        TextButton marketBtn = new TextButton("MARKET", skin);
        TextButton travelBtn = new TextButton("TRAVEL", skin);
        mainBtn.getLabel().setFontScale(0.55f);
        marketBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(mainBtn).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(marketBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        mainBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        TextButton pickNumberBtnLabel = new TextButton("Pick 3 numbers", skin);
        pickNumberBtnLabel.getLabel().setFontScale(0.60f);
        pickNumberBtnLabel.setTouchable(Touchable.disabled);

        TextButton chosenNumbersBtnLabel = new TextButton("Your numbers : ", skin);
        chosenNumbersBtnLabel.getLabel().setFontScale(0.40f);
        chosenNumbersBtnLabel.setTouchable(Touchable.disabled);
        TextButton winningNumbersBtnLabel = new TextButton("Winning numbers : ", skin);
        winningNumbersBtnLabel.getLabel().setFontScale(0.40f);
        winningNumbersBtnLabel.setTouchable(Touchable.disabled);

        thisWeekBtnLable = new TextButton("", skin);
        thisWeekBtnLable.getLabel().setAlignment(Align.center);
        thisWeekBtnLable.getLabel().setFontScale(0.45f);
        thisWeekBtnLable.setTouchable(Touchable.disabled);

        if (!player.hasPlayedLotteryThisWeek()) {
            thisWeekBtnLable.getLabel().setText(" Today's jackpot: " + NumberFormat.getNumberInstance(Locale.US).format(jackpot) + " cr" + "\n Ticket price: " + ticketPrice + " cr");
        } else {
            thisWeekBtnLable.getLabel().setText("  Our offices our closed.\n  Please come back next week!");
        }

        nr1 = new TextButton("1", skin);
        nr1.getLabel().setFontScale(0.75f);
        nr2 = new TextButton("2", skin);
        nr2.getLabel().setFontScale(0.75f);
        nr3 = new TextButton("3", skin);
        nr3.getLabel().setFontScale(0.75f);
        nr4 = new TextButton("4", skin);
        nr4.getLabel().setFontScale(0.75f);
        nr5 = new TextButton("5", skin);
        nr5.getLabel().setFontScale(0.75f);
        nr6 = new TextButton("6", skin);
        nr6.getLabel().setFontScale(0.75f);

        choiceBtnsArray.addAll(nr1, nr2, nr3, nr4, nr5, nr6);

        choice1 = new TextButton("", skin);
        choice1.getLabel().setFontScale(0.75f);
        choice1.setTouchable(Touchable.disabled);
        choice2 = new TextButton("", skin);
        choice2.getLabel().setFontScale(0.75f);
        choice2.setTouchable(Touchable.disabled);
        choice3 = new TextButton("", skin);
        choice3.getLabel().setFontScale(0.75f);
        choice3.setTouchable(Touchable.disabled);

        result1 = new TextButton("", skin);
        result1.getLabel().setFontScale(0.75f);
        result1.setTouchable(Touchable.disabled);
        result2 = new TextButton("", skin);
        result2.getLabel().setFontScale(0.75f);
        result2.setTouchable(Touchable.disabled);
        result3 = new TextButton("", skin);
        result3.getLabel().setFontScale(0.75f);
        result3.setTouchable(Touchable.disabled);

        upperLableTable.add(pickNumberBtnLabel).top().center().padTop(15).padBottom(15).width(400).height(70);
        upperLableTable.row();

        upperNumbers.add(nr1).padBottom(10).padRight(10).height(75).width(75);
        upperNumbers.add(nr2).padBottom(10).padRight(10).height(75).width(75);
        upperNumbers.add(nr3).padBottom(10).height(75).width(75);

        middleNumbers.add(nr4).padBottom(10).padRight(10).height(75).width(75);
        middleNumbers.add(nr5).padBottom(10).padRight(10).height(75).width(75);
        middleNumbers.add(nr6).padBottom(10).height(75).width(75);


        chosenNumbers.add(choice1).height(60).width(60);
        chosenNumbers.add(choice2).height(60).width(60);
        chosenNumbers.add(choice3).height(60).width(60);

        winningnumbers.add(result1).height(60).width(60);
        winningnumbers.add(result2).height(60).width(60);
        winningnumbers.add(result3).height(60).width(60);

        thisWeekTable.add(thisWeekBtnLable).height(103).width(408);

        reset = new TextButton("Reset", skin);
        reset.getLabel().setFontScale(0.55f);
        play = new TextButton("Play", skin);
        play.getLabel().setFontScale(0.55f);

        chosenLableTable.add(chosenNumbersBtnLabel).height(60).width(223);
        chosenNumbersBtnLabel.getLabel().setAlignment(Align.right);
        chosenLableTable.add(chosenNumbers);
        winningLableTable.add(winningNumbersBtnLabel).height(60).width(223);
        winningNumbersBtnLabel.getLabel().setAlignment(Align.right);
        winningLableTable.add(winningnumbers);
        winningLableTable.row();
        winningLableTable.add(reset).fillX();
        winningLableTable.add(play).fillX();

        addListeners();

        rootTable.add(hudLabel).pad(103, 20, 25, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(upperLableTable);
        rootTable.row();
        rootTable.add(upperNumbers);
        rootTable.row();
        rootTable.add(middleNumbers).padBottom(10);
        rootTable.row();
        rootTable.add(thisWeekTable);
        rootTable.row();
        rootTable.add(chosenLableTable);
        rootTable.row();
        rootTable.add(winningLableTable).padBottom(10).padTop(0);
        rootTable.row();
        rootTable.add(lowerButtonTable).padBottom(80);

        stage.addActor(rootTable);
    }

    private void addToArray(String number) {
        if (chosenNumberArray.size < 3) {
            chosenNumberArray.add(number);
            chosenNumberArray.sort();

            if (chosenNumberArray.size == 1) {
                choice1.getLabel().setText(chosenNumberArray.get(0));
            }
            if (chosenNumberArray.size == 2) {
                choice1.getLabel().setText(chosenNumberArray.get(0));
                choice2.getLabel().setText(chosenNumberArray.get(1));
            }
            if (chosenNumberArray.size == 3) {
                choice1.getLabel().setText(chosenNumberArray.get(0));
                choice2.getLabel().setText(chosenNumberArray.get(1));
                choice3.getLabel().setText(chosenNumberArray.get(2));
            }
        }
    }

    private void addListeners() {

        for (final TextButton btn : choiceBtnsArray) {
            btn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    addToArray(btn.getLabel().getText().toString());
                    btn.setDisabled(true);
                    btn.setTouchable(Touchable.disabled);
                    game.gameData.saveGame(vars, player);
                }
            });
        }

        reset.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                choice1.getLabel().setText("");
                choice2.getLabel().setText("");
                choice3.getLabel().setText("");
                if (chosenNumberArray.size > 0) {
                    chosenNumberArray.removeRange(0, chosenNumberArray.size - 1);
                }

                for (TextButton btn : choiceBtnsArray) {
                    btn.setDisabled(false);
                    btn.setTouchable(Touchable.enabled);
                }
            }
        });

        play.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                playPressed = true;
                player.subtractMoney(ticketPrice);
            }
        });
    }

    public void checkWinnings() {

        //1+2+3
        if (chosenNumberArray.get(0).equals(winningNumberArray.get(0))
                && chosenNumberArray.get(1).equals(winningNumberArray.get(1))
                && chosenNumberArray.get(2).equals(winningNumberArray.get(2))) {
            thisWeekBtnLable.getLabel().setText("  Congratulations!\n  You've just won the\n  " + NumberFormat.getNumberInstance(Locale.US).format(jackpot) + " jackpot!!");
            player.addMoney(jackpot);
            assetLoader.moneyWin().play();
            return;

            // 2+3
        } else if ((chosenNumberArray.get(1).equals(winningNumberArray.get(1)) && chosenNumberArray.get(2).equals(winningNumberArray.get(2)))
                && !chosenNumberArray.get(0).equals(winningNumberArray.get(0))) {
            thisWeekBtnLable.getLabel().setText("  Congratulations!\n  You've just won " + NumberFormat.getNumberInstance(Locale.US).format(twoNumbersWinning) + " cr!!");
            player.addMoney(twoNumbersWinning);
            assetLoader.moneyWin().play();
            return;

            //1+2
        } else if ((chosenNumberArray.get(0).equals(winningNumberArray.get(0)) && chosenNumberArray.get(1).equals(winningNumberArray.get(1)))
                && !chosenNumberArray.get(2).equals(winningNumberArray.get(2))) {
            thisWeekBtnLable.getLabel().setText("  Congratulations!\n  You've just won " + NumberFormat.getNumberInstance(Locale.US).format(twoNumbersWinning) + " cr!!");
            player.addMoney(twoNumbersWinning);
            assetLoader.moneyWin().play();
            return;

            //1+3t
        } else if ((chosenNumberArray.get(0).equals(winningNumberArray.get(0)) && chosenNumberArray.get(2).equals(winningNumberArray.get(2)))
                && !chosenNumberArray.get(1).equals(winningNumberArray.get(1))) {
            thisWeekBtnLable.getLabel().setText("  Congratulations!\n  You've just won " + NumberFormat.getNumberInstance(Locale.US).format(twoNumbersWinning) + " cr!!");
            player.addMoney(twoNumbersWinning);
            assetLoader.moneyWin().play();
            return;

            //1
        } else if (chosenNumberArray.get(0).equals(winningNumberArray.get(0))
                || chosenNumberArray.get(1).equals(winningNumberArray.get(1))
                || chosenNumberArray.get(2).equals(winningNumberArray.get(2))) {
            thisWeekBtnLable.getLabel().setText("  Congratulations!\n  You've just won " + NumberFormat.getNumberInstance(Locale.US).format(ticketPrice * 2) + " cr!!");
            player.addMoney(ticketPrice * 2);
            assetLoader.moneyWin().play();
            return;


        } else {
            thisWeekBtnLable.getLabel().setText("  Better luck next time!");
            assetLoader.moneyLose().play();
            return;
        }
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        if (!player.hasPlayedLotteryThisWeek()) {

            thisWeekBtnLable.getLabel().setText("  Today's jackpot : " + NumberFormat.getNumberInstance(Locale.US).format(jackpot) + " cr" + "\n  Ticket price : " + ticketPrice + " cr");

            if (playPressed) {
                timePassed += delta * 5;
                if (timePassed > 2.0f) {
                    result1.getLabel().setText(winningNumberArray.get(0));
                }
                if (timePassed > 4.0f) {
                    result2.getLabel().setText(winningNumberArray.get(1));
                }
                if (timePassed > 6.0f) {
                    result3.getLabel().setText(winningNumberArray.get(2));
                    player.setPlayedLotteryThisWeek(true);
                    checkWinnings();
                }
            }

            if (chosenNumberArray.size < 3) {
                play.setDisabled(true);
                play.setTouchable(Touchable.disabled);
            } else {
                play.setDisabled(false);
                play.setTouchable(Touchable.enabled);
            }

            if (chosenNumberArray.size == 0) {
                reset.setDisabled(true);
                reset.setTouchable(Touchable.disabled);
            } else {
                reset.setDisabled(false);
                reset.setTouchable(Touchable.enabled);
            }

            if (chosenNumberArray.size == 3) {
                for (TextButton btn : choiceBtnsArray) {
                    btn.setDisabled(true);
                    btn.setTouchable(Touchable.disabled);
                }
            }

        } else {
            for (TextButton btn : choiceBtnsArray) {
                btn.setDisabled(true);
                btn.setTouchable(Touchable.disabled);
            }
            play.setDisabled(true);
            play.setTouchable(Touchable.disabled);
            reset.setDisabled(true);
            reset.setTouchable(Touchable.disabled);
            vars.resetSecretNumber();
        }

        if (player.getMoney() < ticketPrice) {
            for (TextButton btn : choiceBtnsArray) {
                btn.setDisabled(true);
                btn.setTouchable(Touchable.disabled);
            }
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
