package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.PlanetType;

import java.text.NumberFormat;
import java.util.Locale;

public class MainScreen extends BaseScreen {

    public MainScreen(final SpaceTrade game) {
        super(game);

        game.gameData.saveGame(vars, player);

        Table rootTable = new Table();
        Table buttonTable = new Table();
        Table planetDetailsLabelTable = new Table();
        rootTable.setFillParent(true);

        Label planetWord = new Label("Current Planet : \n----------------", skin);
        Label planetName = new Label(player.getCurrentPlanet().getPlanetName(), skin);
        Label factionWord = new Label("Ruling Faction : \n----------------", skin);
        Label factionName = new Label(player.getCurrentPlanet().getFactionName(), skin);
        planetWord.setFontScale(0.5f);
        planetName.setFontScale(0.45f);
        factionWord.setFontScale(0.5f);
        factionName.setFontScale(0.45F);

        TextButton marketBtn = new TextButton("Market", skin);
        TextButton travelBtn = new TextButton("Travel", skin);
        TextButton extraBtn = new TextButton(player.getCurrentPlanet().getButtonDisplay(), skin);
        marketBtn.getLabel().setFontScale(0.6f);
        travelBtn.getLabel().setFontScale(0.6f);
        extraBtn.getLabel().setFontScale(0.6f);

        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });

        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        extraBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                visitPlanet();
            }
        });

        if (player.getCurrentPlanet().getType().equals(PlanetType.NONE.getSpecialType())) {
            extraBtn.setVisible(false);
        }

        planetDetailsLabelTable.add(planetWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(planetName).padBottom(25).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionName).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();

        buttonTable.pad(120, 0, 120, 0);

        buttonTable.add(marketBtn).padBottom(20).height(70).width(180);
        buttonTable.row();
        buttonTable.add(travelBtn).padBottom(20).height(70).width(180);
        buttonTable.row();
        buttonTable.add(extraBtn).padBottom(20).height(70).width(180);

        rootTable.add(hudLabel).pad(50, 20, 30, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(planetDetailsLabelTable).padLeft(18).padTop(17).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(buttonTable);

        stage.addActor(rootTable);

    }

    @Override
    public void show() {

        assetLoader.theme().stop();

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.infoBack(), 0, (stage.getHeight() - 15) - 332, stage.getWidth(), 200);
        stage.getBatch().end();

        stage.act();
        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
        }

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
