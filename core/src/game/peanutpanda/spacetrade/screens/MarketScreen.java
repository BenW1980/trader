package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.Commodity;
import game.peanutpanda.spacetrade.gameobjects.PlanetType;

import java.text.NumberFormat;
import java.util.Locale;

public class MarketScreen extends BaseScreen {


    public MarketScreen(final SpaceTrade game) {
        super(game);

        Array<TextButton> commBtns = new Array<>();
        Array<TextButton> onShipBtns = new Array<>();
        Array<TextButton> onPlanetBtns = new Array<>();

        for (Commodity commodity : vars.commodities) {
            onShipBtns.add(new TextButton("" + NumberFormat.getNumberInstance(Locale.US).format(commodity.getAmountOnShip()), skin));

            commBtns.add(new TextButton(commodity.getCommodityName()
                    + "\n\n" + NumberFormat.getNumberInstance(Locale.US).format(commodity.getPrice()) + " cr", skin));

            onPlanetBtns.add(new TextButton("" + String.format("%,d", commodity.getAmountOnPlanet()), skin));
        }

        Table rootTable = new Table();
        rootTable.setFillParent(true);

        Table labelTable = new Table();

        Label onShipLabel = new Label("  On\nShip", skin);
        Label commDescrLabel = new Label("Commodity", skin);
        Label onPlanetLabel = new Label("   On\nPlanet", skin);

        onShipLabel.setFontScale(0.45f);
        commDescrLabel.setFontScale(0.45f);
        onPlanetLabel.setFontScale(0.45f);

        labelTable.add(onShipLabel).padLeft(46);
        labelTable.add(commDescrLabel).padLeft(77).padRight(80);
        labelTable.add(onPlanetLabel).padRight(32);

        Table buttonTable = new Table();

        Table lowerButtonTable = new Table();
        TextButton backButton = new TextButton("MAIN", skin);
        TextButton extraBtn = new TextButton(player.getCurrentPlanet().getButtonDisplay(), skin);
        TextButton travelBtn = new TextButton("TRAVEL", skin);
        backButton.getLabel().setFontScale(0.55f);
        extraBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(backButton).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(extraBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });
        extraBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                visitPlanet();
            }
        });

        if (player.getCurrentPlanet().getType().equals(PlanetType.NONE.getSpecialType())) {
            extraBtn.setVisible(false);
        }

        for (int i = 0; i < commBtns.size; i++) {
            onShipBtns.get(i).getLabel().setFontScale(0.45f);
            commBtns.get(i).getLabel().setFontScale(0.45f);
            onPlanetBtns.get(i).getLabel().setFontScale(0.45f);

            int height = 95;

            buttonTable.add(onShipBtns.get(i)).padBottom(20).height(height).width(90).padRight(5);
            buttonTable.add(commBtns.get(i)).padBottom(20).height(height).width(250);
            buttonTable.add(onPlanetBtns.get(i)).padBottom(20).height(height).width(90).padLeft(5);
            buttonTable.row();

            final int finalI = i;
            onShipBtns.get(i).addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new BuySellScreen(game, vars.commodities.get(finalI)));
                }
            });
            commBtns.get(i).addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new BuySellScreen(game, vars.commodities.get(finalI)));
                }
            });
            onPlanetBtns.get(i).addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new BuySellScreen(game, vars.commodities.get(finalI)));
                }
            });
        }

        ScrollPane scrollPane = new ScrollPane(buttonTable);
        scrollPane.setScrollingDisabled(true, false);
        scrollPane.setFadeScrollBars(false);

        rootTable.add(hudLabel).pad(18, 28, 45, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(labelTable);
        rootTable.row();
        rootTable.add(scrollPane).padTop(25);
        rootTable.row();
        rootTable.add(lowerButtonTable);

        stage.addActor(rootTable);

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.marketLabel(), 10, stage.getHeight() - 215, 460, 65);
        stage.getBatch().end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
