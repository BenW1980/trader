package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;


public class OracleScreen extends BaseScreen {

    private TextButton fortuneBtnLable;
    private TextButton fortuneBtn;

    public OracleScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        Table buttonTable = new Table();
        Table planetDetailsLabelTable = new Table();
        rootTable.setFillParent(true);

        Label planetWord = new Label("Current Planet : \n----------------", skin);
        Label planetName = new Label(player.getCurrentPlanet().getPlanetName(), skin);
        Label factionWord = new Label("Ruling Faction : \n----------------", skin);
        Label factionName = new Label(player.getCurrentPlanet().getFactionName(), skin);
        planetWord.setFontScale(0.5f);
        planetName.setFontScale(0.45f);
        factionWord.setFontScale(0.5f);
        factionName.setFontScale(0.45F);

        Table lowerButtonTable = new Table();
        TextButton mainBtn = new TextButton("MAIN", skin);
        TextButton marketBtn = new TextButton("MARKET", skin);
        TextButton travelBtn = new TextButton("TRAVEL", skin);
        mainBtn.getLabel().setFontScale(0.55f);
        marketBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(mainBtn).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(marketBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        mainBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        planetDetailsLabelTable.add(planetWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(planetName).padBottom(25).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionName).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();

        fortuneBtn = new TextButton("Ask for a blessing", skin);
        fortuneBtn.getLabel().setFontScale(0.6f);

        fortuneBtnLable = new TextButton("  " + fortuneResultTxt(), skin);
        fortuneBtnLable.getLabel().setFontScale(0.50f);
        fortuneBtnLable.getLabel().setAlignment(Align.center);
        fortuneBtnLable.setTouchable(Touchable.disabled);
        fortuneBtnLable.getLabel().setWrap(true);

        if (player.hasWishedForFortune()) {
            fortuneBtn.setTouchable(Touchable.disabled);
            fortuneBtn.setDisabled(true);
        }

        fortuneBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                handleFortune();
                game.gameData.saveGame(vars, player);
            }
        });

        buttonTable.pad(5, 0, 40, 0);

        buttonTable.add(fortuneBtn).padBottom(0).padTop(40).height(70).width(410);

        rootTable.add(hudLabel).pad(50, 20, 20, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(planetDetailsLabelTable).padLeft(18).padTop(28).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(fortuneBtnLable).width(470).height(150).padTop(75);
        rootTable.row();
        rootTable.add(buttonTable);
        rootTable.row();
        rootTable.add(lowerButtonTable).padBottom(40).padTop(10);

        stage.addActor(rootTable);

    }

    private String fortuneResultTxt() {

        String result;

        if (!player.hasWishedForFortune()) {
            result = "The Oracle of " + player.getCurrentPlanet().getPlanetName() + " can grant fortune to those in need.";
        } else {
            result = "The Oracle needs to rest now. Please come back next week!";
        }

        return result;
    }

    private void handleFortune() {

        if (!player.hasWishedForFortune()) {
            player.setWishedForFortune(true);
            fortuneBtnLable.setText(" " + rewardTxt());
            fortuneBtn.setDisabled(true);
            fortuneBtn.setTouchable(Touchable.disabled);
        }
    }

    private String rewardTxt() {

        String text = "";

        if (player.getMoney() < 250 && player.getCargoOnBoard() == 0) {
            int reward = MathUtils.random(80, 900);
            player.addMoney(reward);
            text = "The Oracle takes pity on you \nand gives you " + reward + " credits.";
        } else {
            int random = MathUtils.random(1, 19);
            switch (random) {
                case 1:
                    text = "The Oracle rewards you with a blessing.";
                    break;
                case 2:
                    text = "The Oracle wishes you good luck in your travels.";
                    break;
                case 3:
                    text = "The Oracle thinks you're doing fine on your own.";
                    break;
                case 4:
                    text = "The Oracle sees great things\nin your future.";
                    break;
                case 5:
                    text = "The Oracle looks at you and likes what it sees.";
                    break;
                case 6:
                    text = "The Oracle talks to you about the weather.";
                    break;
                case 7:
                    text = "The Oracle is impressed by the size of your spaceship.";
                    break;
                case 8:
                    text = "The Oracle gives you some welcome financial advice.";
                    break;
                case 9:
                    text = "The Oracle thinks you should make your own good fortune.";
                    break;
                case 10:
                    int money = MathUtils.random(100, 900);
                    player.addMoney(money);
                    text = "The Oracle takes pity on you \nand gives you " + money + " credits.";
                    break;
                case 11:
                    text = "The Oracle tells you to be extra careful on your next trip.";
                    break;
                case 12:
                    text = "The Oracle talks to you about the dangers of gambling.";
                    break;
                case 13:
                    vars.createOracleNumbers();
                    text = "The Oracle tells you that the secret numbers" +
                            " are " + vars.oracleWinningNumbers.get(0) + " - " + vars.oracleWinningNumbers.get(1) + " - " + vars.oracleWinningNumbers.get(2);
                    break;
                case 14:
                    text = "The Oracle advises you to upgrade your ship when you can.";
                    break;
                case 15:
                    text = "The Oracle reminds you about paying your taxes on time.";
                    break;
                case 16:
                    text = "The Oracle tells you to buy low and sell high.";
                    break;
                case 17:
                    text = "The Oracle tells you that the house always wins.";
                    break;
                case 18:
                    int money3 = MathUtils.random(10000, 20000);
                    player.addMoney(money3);
                    text = "The Oracle takes pity on you \nand gives you " + NumberFormat.getNumberInstance(Locale.US).format(money3) + " credits.";
                    break;
                case 19:
                    text = "The Oracle tells you to wait until the last minute to pay your taxes.";
                    break;
            }

        }
        assetLoader.upgrade().play();

        return text;

    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.infoBack(), 0, (stage.getHeight() - 15) - 330, stage.getWidth(), 200);
        stage.getBatch().end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
