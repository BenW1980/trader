package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.ScreenSize;
import game.peanutpanda.spacetrade.SpaceTrade;


public class PrivacyPolicyScreen extends BaseScreen {

    private Label infoBtnLabel;

    public PrivacyPolicyScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        rootTable.setFillParent(true);

        TextButton backBtn = new TextButton("Main Menu", skin);
        backBtn.getLabel().setFontScale(0.6f);

        infoBtnLabel = new Label(aboutTxt(), skin);
        infoBtnLabel.setFontScale(0.5f);
        infoBtnLabel.setAlignment(Align.center);
        infoBtnLabel.setTouchable(Touchable.disabled);
        infoBtnLabel.setWrap(true);
        infoBtnLabel.getStyle().fontColor = new Color(222 / 255f, 251 / 255f, 206 / 255f, 1);

        ScrollPane scrollPane = new ScrollPane(infoBtnLabel);
        scrollPane.setScrollingDisabled(true, false);
        scrollPane.setFadeScrollBars(false);

        backBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
            }
        });

        rootTable.add(scrollPane).width(ScreenSize.WIDTH.getSize() - 20).height(ScreenSize.HEIGHT.getSize() - 150);
        rootTable.row();
        rootTable.add(backBtn).padBottom(20).size(280, 70);

        stage.addActor(rootTable);
    }

    private String aboutTxt() {
        String txt;

        txt = "Privacy Policy\n" +
                "Peanut Panda Apps built this app as a free app. This SERVICE is provided by Peanut Panda Apps at no cost and is intended for use as is.\n" +
                "\n" +
                "This page is used to inform website visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.\n" +
                "\n" +
                "If you choose to use my Service, then you agree to the collection and use of information in relation with this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.\n" +
                "\n" +
                "Information Collection and Use\n" +
                "\n" +
                "For a better experience while using our Service, I may require you to provide us with certain personally identifiable information, including but not limited to your country of origin, the device used and android version number. The information that I request is retained on your device and is not collected by me in any way and used as described in this privacy policy.\n" +
                "\n" +
                "The app does use third party services (Google/Firebase) that may collect information used to identify you.\n" +
                "\n" +
                "Log Data\n" +
                "\n" +
                "I want to inform you that whenever you use my Service, in case of an error in the app I collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your devices’s Internet Protocol (“IP”) address, device name, operating system version, configuration of the app when utilising my Service, the time and date of your use of the Service, and other statistics.\n" +
                "\n" +
                "Service Providers\n" +
                "\n" +
                "I may employ third-party companies (Google/Firebase) due to the following reasons:\n" +
                "\n" +
                "To facilitate our Service;\n" +
                "To provide the Service on our behalf;\n" +
                "To perform Service-related services; or\n" +
                "To assist us in analyzing how our Service is used.\n" +
                "I want to inform users of this Service that these third parties have access to your Personal Information such as country of origin, kind of device, android version number. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.\n" +
                "\n" +
                "Security\n" +
                "\n" +
                "I value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and I cannot guarantee its absolute security.\n" +
                "\n" +
                "Links to Other Sites\n" +
                "\n" +
                "This Service may contain links to other sites such as Twitter. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over, and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.\n" +
                "\n" +
                "Children’s Privacy\n" +
                "\n" +
                "This Services do not address anyone under the age of 13. I do not knowingly collect personal identifiable information from children under 13. In the case I discover that a child under 13 has provided me with personal information, I immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact me so that I will be able to do necessary actions.\n" +
                "\n" +
                "Changes to This Privacy Policy\n" +
                "\n" +
                "I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately, after they are posted on this page.\n" +
                "\n" +
                "Contact Us\n" +
                "\n" +
                "If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact me at peanutpanda.apps@gmail.com.";

        return txt;
    }


    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new TitleScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
