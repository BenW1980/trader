package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.ScreenSize;
import game.peanutpanda.spacetrade.SpaceTrade;


public class RandomTravelEventScreen extends BaseScreen {

    private Label label;
    private TextButton acceptBtn, declineBtn, okButton;
    Table buttonTable;

    public RandomTravelEventScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        rootTable.setFillParent(true);
        Table textTable = new Table();
        buttonTable = new Table();

        label = new Label(text(), skin, "blankLabel");
        label.setFontScale(0.47f);
        label.setAlignment(Align.topLeft);
        label.setTouchable(Touchable.disabled);
        label.setWrap(true);
        label.getStyle().background.setLeftWidth(10);
        label.getStyle().background.setTopHeight(10);
        label.getStyle().font.getData().setLineHeight(55);

        acceptBtn = new TextButton(travelEvent.yesButtonText, skin);
        acceptBtn.getLabel().setFontScale(0.6f);
        declineBtn = new TextButton(travelEvent.noButtonText, skin);
        declineBtn.getLabel().setFontScale(0.6f);
        okButton = new TextButton("", skin);
        okButton.getLabel().setFontScale(0.7f);
        okButton.setVisible(false);

        acceptBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                travelEvent.eventAcceptedResult();
                label.setText(travelEvent.acceptResult);
                buttonTable.clearChildren();
                acceptBtn.remove();
                declineBtn.remove();
                okButton.setVisible(true);
                okButton.setText(travelEvent.okButtonText);
                buttonTable.add(okButton).padTop(45).padBottom(25).width(250).center();
            }
        });

        declineBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new ArrivalScreen(game));
            }
        });

        okButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new ArrivalScreen(game));
            }
        });

        textTable.add(label).size(ScreenSize.WIDTH.getSize() - 50, 500).padTop(80);
        buttonTable.add(acceptBtn).padTop(30).padBottom(15).width(250);
        buttonTable.row();
        buttonTable.add(declineBtn).padTop(16).padBottom(25).width(250);

        rootTable.add(textTable);
        rootTable.row();
        rootTable.add(buttonTable);

        stage.addActor(rootTable);

        game.gameData.setContinueButton(false);

    }

    private String text() {
        return "\n" + travelEvent.randomEventIntroText(player.calculateTaxUrgency());
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        super.render(delta);


        if (travelEvent.doesNotQualify) {
            travelEvent.doesNotQualify = false;
            buttonTable.clearChildren();
            acceptBtn.remove();
            declineBtn.remove();
            okButton.setVisible(true);
            if (travelEvent.fined) {
                travelEvent.fined = false;
                okButton.setText("Ouch!");
            } else {
                okButton.setText("Continue");
            }

            buttonTable.add(okButton).padTop(45).padBottom(25).width(250).center();
        }

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().end();

        stage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
