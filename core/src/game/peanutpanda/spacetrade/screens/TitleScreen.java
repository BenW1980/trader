package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import game.peanutpanda.spacetrade.AssetLoader;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.Vars;


public class TitleScreen implements Screen {

    SpaceTrade game;
    Skin skin;
    AssetLoader assetLoader;
    Stage stage;
    OrthographicCamera camera;
    Viewport viewport;
    Vars vars;

    public TitleScreen(final SpaceTrade game) {
        this.game = game;
        game.vars = new Vars(game.gameData);
        this.vars = game.vars;
        this.camera = game.camera;
        this.viewport = game.viewport;
        this.stage = new Stage(viewport);
        this.assetLoader = game.assLoader;
        this.assetLoader = game.assLoader;
        this.skin = assetLoader.skin();
        this.camera.update();

        vars.initParticleArray(assetLoader);

        Gdx.input.setInputProcessor(stage);

        Table buttonTable = new Table();
        buttonTable.setFillParent(true);

        TextButton continueBtn = new TextButton("Continue Game", skin);
        TextButton newGameBtn = new TextButton("New Game", skin);
        TextButton extraBtn = new TextButton("About Game", skin);
        TextButton highscoreBtn = new TextButton("High Score", skin);
        continueBtn.getLabel().setFontScale(0.6f);
        newGameBtn.getLabel().setFontScale(0.6f);
        extraBtn.getLabel().setFontScale(0.6f);
        highscoreBtn.getLabel().setFontScale(0.6f);

        continueBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                vars.loadGame();
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });

        newGameBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                vars.newGame();
                ((Game) Gdx.app.getApplicationListener()).setScreen(new IntroScreen(game));
            }
        });

        highscoreBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                vars.newGame();
                ((Game) Gdx.app.getApplicationListener()).setScreen(new HighScoreScreen(game));
            }
        });

        extraBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                vars.newGame();
                ((Game) Gdx.app.getApplicationListener()).setScreen(new AboutScreen(game));
            }
        });


        if (!game.gameData.getPrefs().getBoolean("continueOn")) {
            continueBtn.setDisabled(true);
            continueBtn.setTouchable(Touchable.disabled);
        }

        buttonTable.pad(495, 0, 120, 0);

        buttonTable.add(continueBtn).padBottom(20).size(280, 70);
        buttonTable.row();
        buttonTable.add(newGameBtn).padBottom(20).size(280, 70);
        buttonTable.row();
        buttonTable.add(highscoreBtn).padBottom(20).size(280, 70);
        buttonTable.row();
        buttonTable.add(extraBtn).padBottom(20).size(280, 70);

        stage.addActor(buttonTable);
    }

    @Override
    public void show() {

        assetLoader.theme().play();

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        vars.createParticles(delta, assetLoader);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planetsReverse(), 0, 0, stage.getWidth(), stage.getHeight());

        vars.renderParticles(stage, delta);

        stage.getBatch().draw(assetLoader.title(), 35, stage.getHeight() - 290, 410, 203);
        stage.getBatch().end();

        stage.draw();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        game.assLoader.assetManager.dispose();
    }
}
