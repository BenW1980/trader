package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import game.peanutpanda.spacetrade.SpaceTrade;
import game.peanutpanda.spacetrade.gameobjects.TaxUrgency;

public class TransitScreen extends BaseScreen {

    private Sprite shipSprite;
    private Rectangle shipRectangle;

    public TransitScreen(final SpaceTrade game) {
        super(game);
        this.shipSprite = new Sprite(assetLoader.ship());
        if (player.hasTravelledFromBelow()) {
            this.shipRectangle = new Rectangle(stage.getWidth() / 2 - 50, stage.getHeight() + 120, 100, 117);
        } else {
            this.shipRectangle = new Rectangle(stage.getWidth() / 2 - 50, -120, 100, 117);
        }

        this.shipSprite.setBounds(shipRectangle.x, shipRectangle.y, shipRectangle.width, shipRectangle.height);

        taxUrgency = player.calculateTaxUrgency();

        if (taxUrgency != TaxUrgency.CRITICAL) {
            player.isWarnedAboutTaxes = false;
            player.setTaxUrgencyCounter(0);
        }

        if (player.isWarnedAboutTaxes) {
            player.setTaxUrgencyCounter(player.getTaxUrgencyCounter() + 1);
        }


    }

    private void travel(float delta) {
        int speed = 1250;
        if (player.hasTravelledFromBelow()) {
            shipSprite.setFlip(false, true);
            shipRectangle.y -= delta * speed;
            if (shipRectangle.y < -120) {
                player.setTravelledFromBelow(!player.hasTravelledFromBelow());
                if (player.getWeek() > player.MAX_WEEK) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new GameOverScreen(game));
                } else {
                    checkIfTravelEvent();

                }

            }

        } else {
            shipRectangle.y += delta * speed;
            if (shipRectangle.y > stage.getHeight()) {
                player.setTravelledFromBelow(!player.hasTravelledFromBelow());
                if (player.getWeek() > player.MAX_WEEK) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new GameOverScreen(game));
                } else {
                    checkIfTravelEvent();
                }
            }
        }
    }

    private void checkIfTravelEvent() {
        if (MathUtils.random(1, 3) == 1 && player.getWeek() > 3) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new RandomTravelEventScreen(game));
        } else {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new ArrivalScreen(game));
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planetsReverse(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticles(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().draw(shipSprite, shipRectangle.x, shipRectangle.y, shipRectangle.width, shipRectangle.height);
        stage.getBatch().end();

        stage.draw();

        travel(delta);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
