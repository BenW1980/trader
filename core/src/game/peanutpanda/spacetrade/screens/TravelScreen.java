package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import game.peanutpanda.spacetrade.SpaceTrade;

public class TravelScreen extends BaseScreen {

    public TravelScreen(final SpaceTrade game) {
        super(game);

        final Array<ImageButton> planetButtons = new Array<ImageButton>();
        final Array<Label> planetLabels = new Array<Label>();
        final Array<Label> specialLabels = new Array<Label>();

        final ImageButton planet1 = new ImageButton(skin);
        ImageButton planet2 = new ImageButton(skin);
        ImageButton planet3 = new ImageButton(skin);
        ImageButton planet4 = new ImageButton(skin);
        ImageButton planet5 = new ImageButton(skin);
        ImageButton planet6 = new ImageButton(skin);
        ImageButton planet7 = new ImageButton(skin);
        ImageButton planet8 = new ImageButton(skin);

        planetButtons.addAll(planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8);


        for (int i = 0; i < vars.planets.size; i++) {

            if (vars.planets.get(i).getPlanetName().equalsIgnoreCase(player.getCurrentPlanet().getPlanetName())) {
                planetLabels.add(new Label(vars.planets.get(i).getPlanetName(), skin, "planetCurrent"));
                specialLabels.add(new Label(vars.planets.get(i).getType(), skin, "planetCurrent"));
            } else {
                planetLabels.add(new Label(vars.planets.get(i).getPlanetName(), skin, "planetAll"));
                specialLabels.add(new Label(vars.planets.get(i).getType(), skin, "planetAll"));
            }
        }

        planet1.setPosition(70, 680);
        planet2.setPosition(340, 700);
        planet3.setPosition(50, 400);
        planet4.setPosition(320, 390);
        planet5.setPosition(100, 225);
        planet6.setPosition(175, 70);
        planet7.setPosition(340, 190);
        planet8.setPosition(200, 520);

        for (int i = 0; i < planetButtons.size; i++) {
            planetLabels.get(i).setFontScale(0.45f);
            specialLabels.get(i).setFontScale(0.40f);

            int lengthPlanetName = vars.planets.get(i).getPlanetName().length();
            int lengthSpecialName = vars.planets.get(i).getType().length();

            float offset = 0.0f; // hoe hoger, hoe meer naar rechts de tekst

            if (lengthPlanetName <= 3) {
                offset = 6.2f;

            } else if (lengthPlanetName == 4) {
                offset = 5.85f;

            } else if (lengthPlanetName >= 5 && lengthPlanetName <= 7) {
                offset = 4.9f;

            } else if (lengthPlanetName > 7) {
                offset = 3.7f;
            }

            planetLabels.get(i).setPosition(
                    planetButtons.get(i).getX() + ((offset - lengthPlanetName) * 5.0f),
                    planetButtons.get(i).getY() - 35);
            specialLabels.get(i).setPosition(
                    planetButtons.get(i).getX() + ((5.4f - lengthSpecialName) * 5.0f),
                    planetButtons.get(i).getY() - 60);

            final int finalI = i;
            planetButtons.get(i).addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {

                    if (vars.planets.get(finalI).getPlanetName() != player.getCurrentPlanet().getPlanetName()) {
                        assetLoader.engine().setPitch(assetLoader.engine().play(), 1.9f);
                        vars.rnd.randomiseCommodities(vars.commodities);
                        vars.randomizeJackpot();
                        player.setPlayedLotteryThisWeek(false);
                        player.setUpgradeBought(false);
                        player.setPlayedMaxTimes(false);
                        player.setWishedForFortune(false);
                        player.setTimesSpun(0);
                        player.travelTo(vars.planets.get(finalI));
                        player.goToNextWeek();
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new TransitScreen(game));

                    } else { //blijf op huidige planeet
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
                    }
                }
            });

            TextButton backBtn = new TextButton("X", skin);
            int size = 50;
            backBtn.setSize(size, size);
            backBtn.setPosition(stage.getWidth() - size - 10, 10);

            backBtn.addListener(new ClickListener() {
                public void clicked(InputEvent event, float x, float y) {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
                }
            });

            stage.addActor(planetButtons.get(i));
            stage.addActor(planetLabels.get(i));
            stage.addActor(specialLabels.get(i));
            stage.addActor(backBtn);

            vars.initParticleArrayPlanets(assetLoader);
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.planets(), 0, 0, stage.getWidth(), stage.getHeight());
        vars.createParticlesPlanets(delta, assetLoader);
        vars.renderParticles(stage, delta);
        stage.getBatch().end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
