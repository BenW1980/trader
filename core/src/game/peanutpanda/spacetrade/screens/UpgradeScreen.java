package game.peanutpanda.spacetrade.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import game.peanutpanda.spacetrade.SpaceTrade;

import java.text.NumberFormat;
import java.util.Locale;


public class UpgradeScreen extends BaseScreen {

    private TextButton upgradeLabelBtn;
    private TextButton upgradeBtn;

    public UpgradeScreen(final SpaceTrade game) {
        super(game);
        Table rootTable = new Table();
        Table buttonTable = new Table();
        Table planetDetailsLabelTable = new Table();
        rootTable.setFillParent(true);

        Label planetWord = new Label("Current Planet : \n----------------", skin);
        Label planetName = new Label(player.getCurrentPlanet().getPlanetName(), skin);
        Label factionWord = new Label("Ruling Faction : \n----------------", skin);
        Label factionName = new Label(player.getCurrentPlanet().getFactionName(), skin);
        planetWord.setFontScale(0.5f);
        planetName.setFontScale(0.45f);
        factionWord.setFontScale(0.5f);
        factionName.setFontScale(0.45F);

        upgradeBtn = new TextButton("Buy Upgrade", skin);
        upgradeBtn.getLabel().setFontScale(0.6f);

        Table lowerButtonTable = new Table();
        TextButton mainBtn = new TextButton("MAIN", skin);
        TextButton marketBtn = new TextButton("MARKET", skin);
        TextButton travelBtn = new TextButton("TRAVEL", skin);
        mainBtn.getLabel().setFontScale(0.55f);
        marketBtn.getLabel().setFontScale(0.55f);
        travelBtn.getLabel().setFontScale(0.55f);
        lowerButtonTable.add(mainBtn).width(150).padRight(5).padLeft(10);
        lowerButtonTable.add(marketBtn).width(150);
        lowerButtonTable.add(travelBtn).width(150).padLeft(5).padRight(10);
        lowerButtonTable.padBottom(10);

        mainBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MainScreen(game));
            }
        });
        marketBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MarketScreen(game));
            }
        });
        travelBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new TravelScreen(game));
            }
        });

        planetDetailsLabelTable.add(planetWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(planetName).padBottom(25).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionWord).padBottom(5).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();
        planetDetailsLabelTable.add(factionName).fillX().align(Align.left).expandX();
        planetDetailsLabelTable.row();

        upgradeLabelBtn = new TextButton(upgradeText(), skin);
        upgradeLabelBtn.getLabel().setFontScale(0.50f);
        upgradeLabelBtn.getLabel().setAlignment(Align.left);
        upgradeLabelBtn.setTouchable(Touchable.disabled);

        if (player.isUpgradeBought()
                || player.getCargoLevel() == vars.cargoUpgradeSize.size
                || player.getMoney() < vars.cargoUpgradeCost.get(player.getCargoLevel())) {
            upgradeBtn.setDisabled(true);
            upgradeBtn.setTouchable(Touchable.disabled);
        }

        upgradeBtn.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                handleUpgrade();
                game.gameData.saveGame(vars, player);
            }
        });

        buttonTable.pad(5, 0, 40, 0);

        buttonTable.add(upgradeBtn).padBottom(0).padTop(40).height(70).width(350);

        rootTable.add(hudLabel).pad(63, 20, 27, 20).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(planetDetailsLabelTable).padLeft(18).padTop(24).fillX().align(Align.left).expandX();
        rootTable.row();
        rootTable.add(upgradeLabelBtn).width(470).height(150).padTop(78);
        rootTable.row();
        rootTable.add(buttonTable);
        rootTable.row();
        rootTable.add(lowerButtonTable).padBottom(50).padTop(10);

        stage.addActor(rootTable);
    }

    private String upgradeText() {

        String result;

        if (player.getCargoLevel() < vars.cargoUpgradeSize.size) {

            if (!player.isUpgradeBought()) {
                result = "   Upgrade your cargo hold!\n\n" +
                        "   Cost : " + NumberFormat.getNumberInstance(Locale.US).format(vars.cargoUpgradeCost.get(player.getCargoLevel())) + " cr\n" +
                        "   Upgrade :" + vars.cargoUpgradeSize.get(player.getCargoLevel()) + " T\n";
            } else {
                result = "        Sorry, we're sold out.\n\n" +
                        "        Come back next week!";
            }

        } else {
            result = "    No more upgrades available!";
        }

        return result;
    }

    private void handleUpgrade() {

        if (player.getCargoLevel() < vars.cargoUpgradeSize.size) {

            if (!player.isUpgradeBought()) {
                player.subtractMoney(vars.cargoUpgradeCost.get(player.getCargoLevel()));
                player.upgradeCargo(vars.cargoUpgradeSize.get(player.getCargoLevel()));
                player.setUpgradeBought(true);
                upgradeLabelBtn.setText(
                        "   Thank you for your purchase!\n\n" +
                                "   Cargo hold upgraded!");
                player.setCargoLevel(player.getCargoLevel() + 1);
                upgradeBtn.setDisabled(true);
                upgradeBtn.setTouchable(Touchable.disabled);
                assetLoader.upgrade().play();
            }
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        stage.act();

        stage.getBatch().begin();
        stage.getBatch().draw(assetLoader.infoBack(), 0, (stage.getHeight() - 15) - 330, stage.getWidth(), 200);
        stage.getBatch().end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
